// const nodeEnv = process.env.NODE_ENV;
//
// module.exports = {
//     root: true,
//     'env': {
//         'node': true,
//         'browser': true,
//         'es6': true
//     },
//     'extends': ['eslint:recommended', 'plugin:react/recommended'],
//     'parserOptions': {
//         'ecmaFeatures': {
//             'jsx': true
//         },
//         'ecmaVersion': 2018,
//         'sourceType': 'module'
//     },
//     'plugins': [
//         'react'
//     ],
//     'settings': {
//         'react': {
//             'version': 'latest'
//         }
//     },
//     'rules': {
//         'indent': [
//             'warn',
//             2
//         ],
//         'linebreak-style': [
//             'error',
//             'unix'
//         ],
//         'quotes': [
//             'warn',
//             'single'
//         ],
//         'semi': [
//             'warn',
//             'always'
//         ],
//         'no-unused-vars': nodeEnv === 'production' ? 'error' : 'warn',
//         'no-console': nodeEnv === 'production' ? 'error' : 'warn'
//     }
// };
