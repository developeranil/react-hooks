// const path = require('path');
// const webpack = require('webpack');
//
// // files copier
// const CopyWebpackPlugin = require('copy-webpack-plugin');
// // terser minify plugin
// const TerserPlugin = require('terser-webpack-plugin');
// // html helper/copier
// const HtmlWebpackPlugin = require('html-webpack-plugin');
// // webpack css extractor
// const MiniCssExtractPlugin = require('mini-css-extract-plugin');
//
// // use mini css in production
// let getStyleLoader = (argv) => {
//     return (argv.mode === 'production') ? MiniCssExtractPlugin.loader : 'style-loader';
// };
//
// module.exports = (env, argv) => ({
//     entry: {
//         'main': './src/index.js',
//     },
//     output: {
//         path: path.resolve(__dirname, './dist'),
//         publicPath: '/',
//         filename: 'js/[name].js'
//     },
//     plugins: [
//         // webpack css extractor
//         (argv.mode === 'production') ? new MiniCssExtractPlugin({
//             filename: 'css/[name].css'
//         }) : new webpack.DefinePlugin({}),
//         // define global variables
//         new webpack.DefinePlugin({
//             'isDev': (argv.mode === 'development')
//         }),
//         // copy static files
//         new CopyWebpackPlugin([
//             {from: './src/assets', to: 'assets'}
//         ]),
//         new HtmlWebpackPlugin({
//             filename: 'index.html',
//             template: 'src/index.html',
//             inject: true,
//             chunks: ['main']
//         })
//     ],
//     module: {
//         noParse: /lodash/,
//         rules: [
//             {
//                 enforce: 'pre',
//                 test: /\.(js|jsx)$/,
//                 exclude: /(node_modules|bower_components)/,
//                 loader: 'eslint-loader',
//             },
//             {
//                 test: /\.(js|jsx)$/,
//                 exclude: /(node_modules|bower_components)/,
//                 loader: 'babel-loader',
//             },
//             {
//                 test: /\.scss$/,
//                 use: [
//                     getStyleLoader(argv),
//                     'css-loader',
//                     'sass-loader'
//                 ]
//             },
//             {
//                 test: /\.css$/,
//                 use: [
//                     getStyleLoader(argv),
//                     'css-loader'
//                 ]
//             },
//             {
//                 test: /\.(woff|woff2|otf|eot|ttf)$/,
//                 use: [
//                     {
//                         loader: 'file-loader',
//                         options: {
//                             name: '[name].[ext]',
//                             outputPath: './bundle/'
//                         }
//                     }
//                 ]
//             },
//             {
//                 test: /\.(png|jpg|jpeg|gif|svg)$/,
//                 loader: 'file-loader',
//                 options: {
//                     name: '[name].[ext]',
//                     outputPath: './bundle/'
//                 }
//             }
//         ]
//     },
//     resolve: {
//         alias: {
//             '~': path.join(__dirname, './'),
//             '@': path.join(__dirname, './')
//         },
//         extensions: ['*', '.js', '.jsx', '.json', '.scss']
//     },
//     devServer: {
//         historyApiFallback: true,
//         contentBase: path.join(__dirname, 'dist'),
//         overlay: true,
//         noInfo: false,
//         host: '127.0.0.1',
//         port: 8090,
//         /*proxy: {
//             '/api/': 'https://api.gridscraper.com'
//         }*/
//     },
//     performance: {
//         hints: false
//     },
//     optimization: {
//         runtimeChunk: false,
//         minimize: (argv.mode === 'production' && (argv.minify === undefined || argv.minify === 'true')),
//         minimizer: (argv.mode === 'production' && (argv.minify === undefined || argv.minify === 'true')) ? [
//             new TerserPlugin({
//                 test: /\.js($|\?)/i,
//                 cache: false,
//                 parallel: true,
//                 sourceMap: true,
//             })
//         ] : [],
//         splitChunks: {
//             cacheGroups: {
//                 styles: {
//                     name: 'styles',
//                     test: /\.css$/,
//                     chunks: 'all',
//                     enforce: true
//                 }
//             }
//         }
//     },
//     watch: argv.mode !== 'production' || (argv.watch !== undefined && argv.watch === 'true'),
//     devtool: (argv.mode === 'production') ? '' : '#source-map'
// });
