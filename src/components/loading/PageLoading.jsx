import React from 'react';

const PageLoading = (props) => {
    return (
        <div className="text-center">
            <div className={props.isLoading ? 'spinner-border' : 'd-none'} role="status">
                <span className="sr-only">Loading...</span>
            </div>
        </div>
    );
};
export default PageLoading;