import React from 'react';

const ModalPageLoading = (props) => {
    return (
        <div className="text-center">
            <div className={props.isLoading ? 'spinner-border spinner-border' : 'd-none'} role="status">
                <span className="sr-only">Loading...</span>
            </div>
        </div>
    );
};
export default ModalPageLoading;