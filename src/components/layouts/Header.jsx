import React from 'react';
import {Link} from 'react-router-dom';

const Header = () => {
    return (
        <div className="container">
            <div className="panel panel-default">
                <div className="panel-heading">
                    <h3 className="panel-title text-center">
                        Replay App
                    </h3>
                </div>
                <div className="panel-body ">
                    <div className="d-flex  justify-content-center">
                        <h4><Link to="/" className="btn btn-primary mr-2"><i
                            className="fa fa-chart-bar mr-2"/>Dashboard</Link></h4>
                        <h4><Link to="/user" className="btn btn-primary mr-2"><i
                            className="fa fa-users mr-2"/>User</Link></h4>
                        <h4><Link to="/story" className="btn btn-primary mr-2"><i className="fa fa-book mr-2"/>Story
                        </Link></h4>
                        <h4><Link to="/contact" className="btn btn-primary mr-2"><i className="fa fa-mobile mr-2"/>Contact</Link>
                        </h4>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default Header;