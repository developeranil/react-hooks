import React, {useEffect, useState} from 'react';
import axios from "axios";
import CustomException from "../../common/CustomException";
import {Link} from "react-router-dom";
import PageLoading from "../loading/PageLoading";

const Dashboard = () => {
    const data = {contact: 20, story: 40, total: 60};
    const [contacts, setContacts] = useState([]);
    const [books, setBooks] = useState([]);
    const [count, setCount] = useState(data);
    const [isLoading, setIsLoading] = useState(true);
    const [customer, setCustomers] = useState([]);

    useEffect(() => {
        ContactList();
        UserList();
        BookList();
    }, [setIsLoading]);

    const ContactList = () => {
        setIsLoading(true);
        const params = {
            per_page: 5
        };
        const baseUrl = 'http://localhost/laravel-crud-api/api/public/api/contacts';
        axios.get(baseUrl, {params: params}).then(response => {
            if (response.data) {
                setIsLoading(false);
                setContacts(response.data.data || []);
            } else {
                setIsLoading(false);
            }
        }).catch(error => {
            new CustomException('Dashboard', error);
        });
    };

    const UserList = () => {
        setIsLoading(true);
        const params = {
            per_page: 5
        };
        const baseUrl = 'http://localhost/laravel-crud-api/api/public/api/members';
        axios.get(baseUrl, {params: params}).then(response => {
            if (response.data) {
                setIsLoading(false);
                setCustomers(response.data.data || []);
            } else {
                setIsLoading(false);
            }
        }).catch(error => {
            new CustomException('Dashboard', error);
        });
    };


    const BookList = () => {
        setIsLoading(true);
        const params = {
            per_page: 5
        };
        const baseUrl = 'http://localhost/laravel-crud-api/api/public/api/stories';
        axios.get(baseUrl, {params: params}).then(response => {
            if (response.data) {
                setIsLoading(false);
                setBooks(response.data.data || []);
            } else {
                setIsLoading(false);
            }
        }).catch(error => {
            new CustomException('Dashboard', error);
        });
    };

    return (
        <div>
            <div className="wrapper-main-content">
                <nav className="nav flex-column">
                    <div className="sidebar-header">
                        <h3>Replay App</h3>
                    </div>
                </nav>

                <div className="container main-content color-gray-light pt-4">

                    {/*<!--Cards Component--!>*/}
                    <section className="section-cards">
                        {/*Row-start*/}
                        <div className="row">
                            {/*<!--Extract data--!>*/}
                            <div className="col-md-4 col-sm-6 mb-2">
                                <div className="card border-dark bg-danger py-2 mb-3">
                                    <div className="card-body text-white">
                                        <h3 className="text-right"><span className="count"></span>
                                            {count.contact}
                                            <small className="d-block">Contact</small>
                                        </h3>
                                    </div>
                                </div>
                            </div>

                            {/*<!--Site--!>*/}
                            <div className="col-md-4 col-sm-6 mb-2">
                                <div className="card bg-warning border-dark py-2">
                                    <div className="card-body text-white">
                                        <h3 className="text-right"><span className="count"> {count.story}</span>
                                            <small className="d-block">Story</small>
                                        </h3>
                                    </div>
                                </div>
                            </div>

                            {/*<!--Url--!>*/}
                            <div className="col-md-4 col-sm-6 mb-3">
                                <div className="card bg-info py-2">
                                    <div className="card-body border-dark text-white">
                                        <h3 className="text-right"><span className="count"> {count.total}</span>
                                            <small className="d-block">Total</small>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row ">
                            <div className="card col-md-12 mb-3">
                                <div className="card-body">
                                    <div className="panel panel-default">
                                        <PageLoading isLoading={isLoading}/>
                                        <div className="panel-heading">
                                            <h3 className='text-right'><small>Contact List</small></h3>
                                        </div>
                                        <div className="panel-body ">
                                            {contacts.length > 0 ? (
                                                <div>
                                                    <table className="table table-striped responsive">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col"><i className="fa fa-star"/></th>
                                                            <th scope="col">Name</th>
                                                            <th scope="col">email</th>
                                                            <th scope="col">Address</th>
                                                            <th scope="col">Phone</th>
                                                            <th scope="col">DOB</th>
                                                            <th scope="col">Description</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        {contacts.map(contact => (
                                                            <tr key={contact.id}>
                                                                <td>{contact.id}</td>
                                                                <td><Link
                                                                    to={'books/' + contact.id}>{contact.name}</Link>
                                                                </td>
                                                                <td>{contact.email}</td>
                                                                <td>{contact.address}</td>
                                                                <td>{contact.phone}</td>
                                                                <td>{contact.dob}</td>
                                                                <td>{contact.description}</td>
                                                            </tr>
                                                        ))}
                                                        </tbody>
                                                    </table>
                                                    <div className="card-footer com-card-footer">
                                                        <Link to={'/contact'}
                                                              className="btn btn-primary py-2 px-3 mx-0 float-right">
                                                            < i className=" fa fa-eye mr-1"/>View Contact</Link>
                                                    </div>
                                                </div>
                                            ) : (
                                                <tr>
                                                    <td colSpan={3}>No Contacts</td>
                                                </tr>
                                            )}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="card col-md-12 mb-3">
                                <div className="card-body">
                                    <div className="panel panel-default">
                                        <PageLoading isLoading={isLoading}/>
                                        <div className="panel-heading">
                                            <h3 className='text-right'><small>Stories List</small></h3>
                                        </div>
                                        <div className="panel-body ">
                                            {books.length > 0 ? (
                                                <div>
                                                    <table className="table table-striped responsive">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col"><i className="fa fa-star"/></th>
                                                            <th scope="col">Name</th>
                                                            <th scope="col">title</th>
                                                            <th scope="col">Address</th>
                                                            <th scope="col">Description</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        {books.map(book => (
                                                            <tr key={book.id}>
                                                                <td>{book.id}</td>
                                                                <td><Link to={'stories/' + book.id}>{book.name}</Link>
                                                                </td>
                                                                <td>{book.title}</td>
                                                                <td>{book.address}</td>
                                                                <td>{book.description ? book.description : 'N/A'}</td>
                                                            </tr>
                                                        ))}

                                                        </tbody>
                                                    </table>
                                                    <div className="card-footer com-card-footer">
                                                        <Link to={'/story'}
                                                              className="btn btn-primary py-2 px-3 mx-0 float-right">
                                                            < i className=" fa fa-eye mr-1"/>View Story</Link>
                                                    </div>
                                                </div>
                                            ) : (
                                                <tr>
                                                    <td colSpan={3}>No Stories</td>
                                                </tr>
                                            )}
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        {/*<!---Row-end---!>*/}
                    </section>
                </div>
            </div>
        </div>
    );
};
export default Dashboard;