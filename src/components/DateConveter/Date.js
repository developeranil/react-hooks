import React, {useRef, useState} from 'react';
import PropTypes from 'prop-types';
import DateList from "./DateList";

const Date = () => {
    const Data = [
        {id: 1, month: 'Baishakh', day: 20, year: 2076}];
    //state //setState   //initialState value
    const initialFormState = {id: null, month: '', day: '', year: ''};
    // state       //setState
    const [date, setDate] = useState(initialFormState);
    const [dates, setDates] = useState(Data);

    const per_page = useRef();
    const Page = ['AD to BS', 'BS to AD'];
    const [perpage] = useState(Page);
    const [page, setPage] = useState('');

    // English Date
    const Year = useRef();
    const EYear = [1950, 1951, 1952, 1953, 1954, 1955, 1956, 1957, 1958, 1959,
        1960, 1961, 1962, 1963, 1964, 1965, 1966, 1967, 1968, 1969, 1970, 1971,
        1972, 1973, 1974, 1975, 1976, 1977, 1978, 1979, 1980, 1981, 1982, 1983,
        1944, 1985, 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995,
        1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007,
        2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019,
        2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030, 2031,
        2032, 2033, 2034, 2035, 2036, 2037, 2038, 2039, 2040, 2041, 2042, 2043,
        2044, 2045, 2046, 2047, 2088, 2049, 2050
    ];
    const [peryear] = useState(EYear);
    const [year, setYear] = useState('');

    const Month = useRef();
    const EMonth = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    const [permonth] = useState(EMonth);
    const [month, setMonth] = useState('');

    const Day = useRef();
    const EDay = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32];
    const [perday] = useState(EDay);
    const [day, setDay] = useState('');

    //Nepali date
    const Sal = useRef();
    const NSal = [1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007,
        2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019,
        2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030, 2031,
        2032, 2033, 2034, 2035, 2036, 2037, 2038, 2039, 2040, 2041, 2042, 2043,
        2044, 2045, 2046, 2047, 2088, 2049, 2050, 2051, 2052, 2053,
        2054, 2055, 2056, 2057, 2058, 2059, 2060, 2061, 2062, 2063,
        2064, 2065, 2066, 2067, 2068, 2069, 2070, 2071, 2072, 2073,
        2074, 2075, 2076, 2077, 2078, 2079, 2080, 2081, 2082, 2083,
        2084, 2085, 2086, 2087, 2088, 2089, 2090, 2091, 2092, 2093,
        2094, 2095, 2096, 2077, 2098, 2099, 2100,
    ];
    const [persal] = useState(NSal);
    const [sal, setSal] = useState('');

    const Mahina = useRef();
    const NMahina = ['Baishakh', 'Jestha', 'Asar', 'Shrawan', 'Bhadau', 'Aswin', 'Kartik', 'Mansir', 'Poush', 'Magh', 'Falgun', 'Chaitra'];
    const [permahina] = useState(NMahina);
    const [mahina, setMahina] = useState('');

    const Din = useRef();
    const NDin = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32];
    const [perdin] = useState(NDin);
    const [din, setDin] = useState('');


    //handlePage
    const handlePage = (event) => {
        event.preventDefault();
        let selected = event.target.value;
        if (selected !== page) {
            setPage(event.target.value);
        }
    };

    //handleEnglishDate
    const handleMonth = (event) => {
        event.preventDefault();
        let selected = event.target.value;
        if (selected !== month) {
            setMonth(event.target.value);
        }
    };

    const handleDay = (event) => {
        event.preventDefault();
        let selected = event.target.value;
        if (selected !== day) {
            setDay(event.target.value);
        }
    };
    const handleYear = (event) => {
        event.preventDefault();
        let selected = event.target.value;
        if (selected !== year) {
            setYear(event.target.value);
        }
    };

    //handleNepaliDate
    const handleMahina = (event) => {
        event.preventDefault();
        let selected = event.target.value;
        if (selected !== mahina) {
            setMahina(event.target.value);
        }
    };

    const handleDin = (event) => {
        event.preventDefault();
        let selected = event.target.value;
        if (selected !== din) {
            setDin(event.target.value);
        }
    };

    const handleSal = (event) => {
        event.preventDefault();
        let selected = event.target.value;
        if (selected !== sal) {
            setSal(event.target.value);
        }
    };

    const handleDate = (event) => {
        if (event) event.preventDefault();
        const date = {
            year: Year.current.value,
            month: Month.current.value,
            day: Day.current.value,
        };
        if (!date.year || !date.month || !date.day) return;
        // update state value
        setDate(initialFormState);
        // add date
        date.id = dates.length + 1;
        // update  date value to the state
        setDates([...dates, date]);
    };

    return (
        <div>
            <div className="form-group">
                <form onSubmit={handleDate}>
                    <label htmlFor="sel1">Date Converter:</label>
                    <select ref={per_page}
                            onChange={(event) => handlePage(event)}
                            className="form-control">
                        {perpage.map((page, i) => {
                            return <option key={i} defaultValue={page}>{page || 'AD to BS'}</option>;
                        })}
                    </select>
                    {page === 'AD to BS' ? (
                        <div className="d-flex justify-content-between">
                            <div className="d-flex flex-row justify-content-center align-items-center">
                                <select ref={Year}
                                        onChange={(event) => handleYear(event)}
                                        className="form-control">
                                    {peryear.map((year, i) => {
                                        return <option key={i} defaultValue={{year: 2019}}>{year}</option>;
                                    })}
                                </select>
                            </div>
                            <div className="d-flex flex-row justify-content-center align-items-center">
                                <select ref={Month}
                                        onChange={(event) => handleMonth(event)}
                                        className="form-control">
                                    {permonth.map((month, i) => {
                                        return <option key={i} defaultValue={month}>{month || 'Jan'}</option>;
                                    })}
                                </select>
                            </div>
                            <div className="d-flex flex-row justify-content-center align-items-center mb-3">
                                <select ref={Day}
                                        onChange={(event) => handleDay(event)}
                                        className="form-control">
                                    {perday.map((day, i) => {
                                        return <option key={i} defaultValue={day}>{day || 1}</option>;
                                    })}
                                </select>
                            </div>
                        </div>
                    ) : (
                        <div className="d-flex justify-content-between">
                            <div className="d-flex flex-row justify-content-center align-items-center">
                                <select ref={Sal}
                                        onChange={(event) => handleSal(event)}
                                        className="form-control">
                                    {persal.map((sal, i) => {
                                        return <option key={i} defaultValue={{sal: 2076}}>{sal}</option>;
                                    })}
                                </select>
                            </div>
                            <div className="d-flex flex-row justify-content-center align-items-center">
                                <select ref={Mahina}
                                        onChange={(event) => handleMahina(event)}
                                        className="form-control">
                                    {permahina.map((mahina, i) => {
                                        return <option key={i} defaultValue={mahina}>{mahina || 'Baikash'}</option>;
                                    })}
                                </select>
                            </div>
                            <div className="d-flex flex-row justify-content-center align-items-center mb-3">
                                <select ref={Din}
                                        onChange={(event) => handleDin(event)}
                                        className="form-control">
                                    {perdin.map((din, i) => {
                                        return <option key={i} defaultValue={din}>{din || 1}</option>;
                                    })}
                                </select>
                            </div>
                        </div>
                    )}
                    <div className="d-flex flex-row justify-content-center align-items-center mb-2">
                        <button type="submit" className=" btn btn-primary mr-2"><i
                            className="fa fa-calendar-week mr-1"/>Date
                            Converter
                        </button>
                    </div>
                </form>
                <div className="container">
                    <div className="panel panel-default">
                        {dates.length > 0 ? (
                            <table className="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">Month</th>
                                    <th scope="col">Day</th>
                                    <th scope="col">Year</th>
                                </tr>
                                </thead>
                                <tbody>
                                {dates.map(date => (
                                    <tr key={date.id}>
                                        <td width="20%">{date.month}</td>
                                        <td width="20%">{date.day}</td>
                                        <td width="20%">{date.year}</td>
                                    </tr>
                                ))}

                                </tbody>
                            </table>
                        ) : (
                            <tr>
                                <td colSpan={3}>No Dates</td>
                            </tr>
                        )}
                    </div>
                </div>
            </div>
        </div>
    );

};
Date.propTypes = {
    addDate: PropTypes.func,
};
export default Date;
