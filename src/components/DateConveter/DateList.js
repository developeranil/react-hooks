import React, {useState} from 'react';
import Date from "./Date";

const DateList = () => {
    const Data = [
        {id: 1, month: 'Baishakh', day: 20, year: 2076}];
    //state //setState   //initialState value
    const [dates, setDates] = useState(Data);

    return (
        <div className="container">
            <div className="panel panel-default">
                {dates.length > 0 ? (
                    <table className="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">Month</th>
                            <th scope="col">Day</th>
                            <th scope="col">Year</th>
                        </tr>
                        </thead>
                        <tbody>
                        {dates.map(date => (
                            <tr key={date.id}>
                                <td width="20%">{date.month}</td>
                                <td width="20%">{date.day}</td>
                                <td width="20%">{date.year}</td>
                            </tr>
                        ))}

                        </tbody>
                    </table>
                ) : (
                    <tr>
                        <td colSpan={3}>No Dates</td>
                    </tr>
                )}
            </div>
        </div>
    );
};

export default DateList;