import React, {useRef, useState} from 'react';
import PropTypes from 'prop-types';
import axios from "axios";
import CustomException from "../../common/CustomException";
import ModalPageLoading from "../loading/ModalPageLoading";

const AddContact = (props) => {

    const name = useRef();
    const email = useRef();
    const address = useRef();
    const phone = useRef();
    const dob = useRef();
    const description = useRef();
    const [isLoading, setIsLoading] = useState(false);


    //formSubmit
    const handleSubmit = (event) => {
        setIsLoading(true);
        event.preventDefault();
        let contact = {
            name: name.current.value,
            email: email.current.value,
            address: address.current.value,
            phone: phone.current.value,
            dob: dob.current.value,
            description: description.current.value,
        };
        const baseUrl = 'http://localhost/laravel-crud-api/api/public/api/contacts';
        axios.post(baseUrl, contact).then(response => {
            // check response data
            if (response.data) {
                setIsLoading(false);
                // passing props to parent to child component
                props.addContact();
                //clearing an input field after form submit
                name.current.value = '';
                email.current.value = '';
                address.current.value = '';
                phone.current.value = '';
                dob.current.value = '';
                description.current.value = '';
                // close modal
                document.getElementById('btnCloseAddContactModal').click();
            }
        }).catch(error => {
            new CustomException('ContactAdd', error);
        });
    };

    AddContact.propTypes = {
        addContact: PropTypes.func,
    };


    return (
        <div className="modal fade" id="AddContactModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header bg-primary text-white">
                        <h5 className="modal-title" id="exampleModalLongTitle"><i className="fa fa-address-book mr-2"/>Add
                            Contact</h5>
                        <button type="button" id="btnCloseAddContactModal" className="close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <ModalPageLoading isLoading={isLoading}/>
                        <form onSubmit={handleSubmit}>
                            <div className="form-group">
                                <label htmlFor="title"> Full Name:</label>
                                <input type="text" ref={name} className="form-control" name="crawler_name"
                                       required/>
                            </div>
                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label htmlFor="title">Phone:</label>
                                    <input type="text" ref={phone} className="form-control" name="crawler_name"
                                           required/>
                                </div>
                                <div className="form-group col-md-6">
                                    <label htmlFor="title">Email:</label>
                                    <input type="email" ref={email} className="form-control" name="crawler_name"
                                           required/>
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label htmlFor="title">Address:</label>
                                    <input type="text" ref={address} className="form-control" name="crawler_name"/>
                                </div>
                                <div className="form-group col-md-6">
                                    <label htmlFor="title">DOB:</label>
                                    <input type="date" ref={dob} className="form-control" name="dob"/>
                                </div>
                            </div>
                            <div className="form-group">
                                <label htmlFor="exampleFormControlTextarea1">Description</label>
                                <textarea className="form-control" ref={description} id="exampleFormControlTextarea1"
                                          rows="3"></textarea>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-outline-secondary" data-dismiss="modal"><i
                                    className="mr-1  fa fa-times"/>Close
                                </button>
                                <button type="submit" className="btn btn-primary"><i className="fa fa-check mr-2"/>Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default AddContact;