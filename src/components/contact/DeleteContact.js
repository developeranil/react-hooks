import React,{useEffect,useState} from 'react';
import PropTypes from 'prop-types';
import axios from "axios";
import CustomException from "../../common/CustomException";
import ModalPageLoading from "../loading/ModalPageLoading";

const DeleteContact = (props) => {

    const [contactId, setContactId] = useState(props.contactId);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(
        () => {
            setContactId(props.contactId);
        },
        [props, setContactId, contactId]
    );
    const handleClick = () => {
        setIsLoading(true);
        const contactId = props.contactId;
        console.log(contactId);

        axios.delete('http://localhost/laravel-crud-api/api/public/api/contacts/' + contactId).then(response => {
            console.log(response);
            // check response.data
            if (response.data) {
                setIsLoading(false);
                // passing linkId and crawler id to child to parent component
                props.deleteContact(props.contactId);
                // close modal
                document.getElementById('btnCloseContactDeleteModal').click();
            }
        }).catch(function (error) {
            new CustomException('DeleteContact', error);
        });
    };
    return (
        <section className="section-modal">
            <div className="modal fade" id="DeleteContactModal" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content position-relative">
                        <div className="modal-header bg-primary text-white">
                            <h5 className="modal-title" id="exampleModalLongTitle"><i className="fa fa-trash mr-2"/>Delete
                                User</h5>
                            <button type="button" id="btnCloseContactDeleteModal" className="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body pb-0 position-relative">
                            <ModalPageLoading isLoading={isLoading}/>
                            <p className="text-bold"><strong>Are you sure?</strong></p>
                            <p className="text-muted">Do you really want to delete these Contact?</p>
                        </div>
                        <div className="modal-footer">
                            <button type="submit" className="btn btn-danger" onClick={handleClick}>
                                <i className="fa fa-trash mr-1"/>Delete
                            </button>
                            <button type="button" className="btn btn-outline-secondary" data-dismiss="modal"><i
                                className="fa fa-times mr-1"/> Cancel
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};
DeleteContact.propTypes = {
    deleteContact: PropTypes.func,
    contactId: PropTypes.number
};
export default DeleteContact;