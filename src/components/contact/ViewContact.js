import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import axios from "axios";
import CustomException from "../../common/CustomException";
import PageLoading from "../loading/PageLoading";

const ViewContact = (props) => {

    const [contacts, setContacts] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    ViewContact.propTypes = {
        match: PropTypes.shape({
            params: PropTypes.shape({
                id: PropTypes.number
            })
        })
    };

    useEffect(() => {
        let contactId = props.match.params.id;

        axios.get('http://localhost/laravel-crud-api/api/public/api/contacts/' + contactId).then(response => {
            let contacts = response.data;
            // check response
            if (response.data) {
                setIsLoading(false);
                setContacts(contacts || [])
            }
        }).catch(function (error) {
            new CustomException('ViewContact', error);
        });
    }, [setContacts]);

    return (
        <div className="container">
            <div className="panel panel-default">
                <h1><i className="fa fa-address-book"></i>View Contact</h1>
                <div className="panel-heading">
                </div>
                <div className="panel-body ">
                    <PageLoading isLoading={isLoading}/>
                    <div className="d-flex">
                        <h4><Link to="/contact" className="btn btn-primary mr-2 float-right"><i
                            className="fa fa-arrow-left"></i> Back</Link></h4>
                    </div>
                    <div className="table-responsive">
                        <table className="table table-hover">
                            <tbody>
                            <tr>
                                <th width="28%" scope="row" className="p-2"> Name:</th>
                                <td className="p-2">{contacts.name}</td>
                            </tr>
                            <tr>
                                <th scope="row" className="p-2">Email:</th>
                                <td className="p-2">{contacts.email}</td>
                            </tr>
                            <tr>
                                <th scope="row" className="p-2">Address:</th>
                                <td className="p-2">{contacts.address}</td>
                            </tr>
                            <tr>
                                <th scope="row" className="p-2">Phone:</th>
                                <td className="p-2">{contacts.phone}</td>
                            </tr>
                            <tr>
                                <th scope="row" className="p-2">DOB:</th>
                                <td className="p-2">{contacts.dob}</td>
                            </tr>
                            <tr>
                                <th scope="row" className="p-2">Description:</th>
                                <td className="p-2">{contacts.description ? contacts.description : 'N/A'}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ViewContact;