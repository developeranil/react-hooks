import React, {useState, useEffect, useRef} from 'react';
import axios from "axios";
import CustomException from "../../common/CustomException";
import ModalPageLoading from "../loading/ModalPageLoading";

const EditContact = (props) => {
    //int the data
    const [currentContact, setCurrentContact] = useState(props.contact);
    const [isLoading, setIsLoading] = useState(false);

    //using useEffect
    useEffect(() => {
        setCurrentContact(props.contact);
    }, [props, setCurrentContact]);

    // useRef
    const name = useRef();
    const email = useRef();
    const address = useRef();
    const phone = useRef();
    const dob = useRef();
    const description = useRef();

    //formSubmit
    let handleSubmit = (e) => {
        setIsLoading(true);
        e.preventDefault();
        let contact = {
            name: name.current.value,
            email: email.current.value,
            address: address.current.value,
            phone: phone.current.value,
            dob: dob.current.value,
            description: description.current.value,
        };

        const ContactId = props.contact.id;
        axios.patch('http://localhost/laravel-crud-api/api/public/api/contacts/' + ContactId, contact).then(response => {
            // check response data
            if (response.data) {
                setIsLoading(false);
                // passing data to child through props
                props.updateContact(props.contact, props.contact.id);
                // hide modal
                document.getElementById('btnCloseEditContactModal').click();
            }
        }).catch(error => {
            new CustomException('EditContact', error);
        });
    };

    return (
        <div className="modal fade" id="EditContactModal" tabIndex="-1" role="dialog"
             aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header bg-primary text-white">
                        <h5 className="modal-title" id="exampleModalLongTitle"><i className="fa fa-address-book mr-2"/>Edit
                            Contact</h5>
                        <button type="button" id="btnCloseEditContactModal" className="close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <ModalPageLoading isLoading={isLoading}/>
                        <form onSubmit={handleSubmit}>
                            <div className="form-group">
                                <label htmlFor="title"> Name:</label>
                                <input type="text" ref={name} className="form-control" name="name"
                                       defaultValue={currentContact.name}
                                       required/>
                            </div>
                            <div className="form-row">
                                <div className="form-group col-mod-6">
                                    <label htmlFor="title">Phone:</label>
                                    <input type="text" ref={phone} className="form-control" name="phone"
                                           defaultValue={currentContact.phone}
                                           required/>
                                </div>
                                <div className="form-group col-md-6">
                                    <label htmlFor="title">Email:</label>
                                    <input type="email" ref={email} className="form-control" name="email"
                                           defaultValue={currentContact.email}
                                           required/>
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label htmlFor="title">Address:</label>
                                    <input type="text" ref={address} className="form-control" name="address"
                                           defaultValue={currentContact.address}
                                           required/>
                                </div>
                                <div className="form-group col-mod-6">
                                    <label htmlFor="title">DOB:</label>
                                    <input type="date" ref={dob} className="form-control" name="dob"
                                           defaultValue={currentContact.dob}
                                           required/>
                                </div>
                            </div>
                            <div className="form-group">
                                <label htmlFor="title">Description:</label>
                                <input type="text" ref={description} className="form-control" name="description"
                                       defaultValue={currentContact.description}
                                       required/>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-outline-secondary" data-dismiss="modal"><i
                                    className="mr-1  fa fa-times"/>Close
                                </button>
                                <button type="submit" className="btn btn-primary"><i className="fa fa-check mr-2"/>Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default EditContact;