import React, {useEffect, useRef, useState} from 'react';
import {Link} from 'react-router-dom';
import AddContact from "./AddContact";
import DeleteContact from "./DeleteContact";
import EditContact from "./EditContact";
import axios from "axios";
import CustomException from "../../common/CustomException";
import PageLoading from "../loading/PageLoading";

const ContactList = () => {
    const [contacts, setContacts] = useState([]);
    const per_page = useRef();
    const search = useRef();
    //perpage
    const Page = [5, 10, 20, 50];
    const [perpage, setPerpage] = useState(Page);
    const [isLoading, setIsLoading] = useState(true);
    const [page, setPage] = useState('');

    // pagination
    const [pageUrl, setPageUrl] = useState('http://localhost/laravel-crud-api/api/public/api/contacts');
    const [first, setFirst] = useState('');
    const [last, setLast] = useState('');
    const [prev, setPrev] = useState('');
    const [next, setNext] = useState('');
    const [nextPage, setNextPage] = useState('');
    const [prevPage, setPrevPage] = useState('');
    const [currentPage, setCurrentPage] = useState('');
    const [lastPage, setLastPage] = useState('');
    const [to, setTo] = useState('');
    const [from, setFrom] = useState('');
    const [total, setTotal] = useState('');

    const [selectedContactId, setSelectedContactId] = useState(null);

    const [currentContact, setCurrentContact] = useState({
        id: null,
        name: '',
        email: '',
        address: '',
        phone: '',
        photo_url: '',
        dob: '',
        description: ''
    });


    useEffect(() => {
        Contact();
    }, [setPerpage, first, last, prevPage, from, setPageUrl, pageUrl]);

    //handle pagination
    const handlePager = event => {
        event.preventDefault();
        setPageUrl(event.target.href);
        Contact();
    };

    //handle page
    const handlePage = event => {
        let selected = event.target.value;
        if (selected !== page) {
            setPage(event.target.value);
            Contact();
        }
    };

    const Contact = () => {
        setIsLoading(true);
        const params = {
            per_page: per_page.current.value,
            keyword: search.current.value,
        };
        const baseUrl = (pageUrl == null) ? 'http://localhost/laravel-crud-api/api/public/api/contacts' : pageUrl;
        axios.get(baseUrl, {params: params}).then(response => {
            const contact = response.data.links;
            const contacts = response.data.meta;
            if (response.data) {
                setIsLoading(false);
                setContacts(response.data.data || []);
                setCurrentPage(contacts.current_page);
                setLastPage(contacts.last_page);
                setFrom(contacts.form);
                setTo(contacts.to);
                setTotal(contacts.total);
                setFirst(contact.first);
                setLast(contact.last);
                setPrev(contact.prev);
                setNext(contact.next);
                setNextPage(contacts.current_page + 1);
                setPrevPage(contacts.current_page - 1);

            } else {
                setIsLoading(false);
            }
        }).catch(error => {
            new CustomException('Dashboard', error);
        });
    };

    // add contact
    function addContact() {
        setIsLoading(true);
        Contact();
    }


    // delete contact
    function deleteContact(contactId) {
        setIsLoading(false);
        setContacts(contacts.filter(contact => contact.id !== contactId));
        Contact();
    }

    // update contact
    function updateContact(id, updateContact) {
        setIsLoading(false);
        setContacts(contacts.map(contact => (contact.id === id ? updateContact : contact)))
        Contact();
    }

    return (
        <div className="container">
            <section className="com-maincontent-header d-flex w-100">
                <div className="col-sm-4 text-right">
                    <ol className="breadcrumb breadcrumb-list">
                        <li className="breadcrumb-item"><Link to="/">Dashboard</Link>
                        </li>
                        <li className="breadcrumb-item active">Contact
                        </li>
                    </ol>
                </div>
            </section>
            <div className="panel panel-default">
                <h1><i className="fa fa-address-book"/> ContactLists</h1>
                <div className="panel-heading">
                </div>
                <div className="panel-body ">
                    <div className="d-flex">
                        <h4>
                            <button type="submit" className=" btn btn-primary mr-2" data-toggle="modal"
                                    data-target="#AddContactModal"><i className="fa fa-plus mr-1"/>Add Contact
                            </button>
                        </h4>
                        <h4><Link to="/" className="btn btn-primary mr-2 float-right"><i
                            className="fa fa-arrow-left"/> Back</Link></h4>
                    </div>
                    <div className="d-flex justify-content-between">
                        <div className="d-flex flex-row justify-content-center align-items-center">
                            <label className="mr-2">Perpage:</label>
                            <select ref={per_page}
                                    onChange={(event) => handlePage(event)}
                                    className="form-control">
                                {perpage.map((page, i) => {
                                    return <option key={i} defaultValue={page}>{page || 10}</option>;
                                })}
                            </select>
                        </div>
                        <div className="form-group d-flex flex-row float-right w-50">
                            <label htmlFor="search" className="mr-2 mt-2">Search:</label>
                            <input type="text" className="form-control search-input"
                                   ref={search} aria-describedby="search"
                                   onChange={Contact} placeholder="search"/>
                        </div>
                    </div>
                    <PageLoading isLoading={isLoading}/>
                    {contacts.length > 0 ? (
                        <div>
                            <table className="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col"><i className="fa fa-star"/></th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Address</th>
                                    <th scope="col">Phone</th>
                                    <th scope="col">DOB</th>
                                    <th scope="col">Description</th>
                                    <th scope="col" className="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                {contacts.map(contact => (
                                    <tr key={contact.id}>
                                        <td>{contact.id}</td>
                                        <td><Link to={'data/' + contact.id}>{contact.name}</Link></td>
                                        <td>{contact.email}</td>
                                        <td>{contact.address}</td>
                                        <td>{contact.phone}</td>
                                        <td>{contact.dob}</td>
                                        <td>{contact.description}</td>
                                        <td className="text-center d-flex">
                                            <Link to={'data/' + contact.id} className="btn btn-primary mr-2"><i
                                                className="fa fa-eye"/></Link>
                                            <button onClick={() => {
                                                setCurrentContact(contact);
                                            }} className="btn btn-success mr-2" data-toggle="modal"
                                                    data-target="#EditContactModal"><i className="fa fa-pen"/></button>
                                            <button onClick={() => {
                                                setSelectedContactId(contact.id);
                                            }} className="btn btn-danger" data-toggle="modal"
                                                    data-target="#DeleteContactModal"><i className="fa fa-times"/>
                                            </button>
                                        </td>
                                    </tr>
                                ))}

                                </tbody>
                            </table>
                            <div className=" d-flex justify-content-start"><label
                                className="mr-2">Page {currentPage} of {lastPage}</label>
                            </div>
                            <nav>
                                <ul className="pagination justify-content-end">
                                    <li className={prev === null ? 'page-item disabled' : 'page-item'}>
                                        <a className="page-link" href={prev} onClick={handlePager}
                                           tabIndex="-1">Previous</a>
                                    </li>
                                    <li className="page-item active">
                                        <a className="page-link" href={currentPage}
                                           onClick={handlePager}>{currentPage}<span
                                            className="sr-only">(current)</span></a>
                                    </li>
                                    <li className={to === total ? 'hide-element' : 'page-item'}><a
                                        className="page-link"
                                        onClick={handlePager}
                                        href={next}>{nextPage}</a>
                                    </li>
                                    <li className={next === null ? 'page-item disabled' : 'page-item'}>
                                        <a className="page-link" href={next} onClick={handlePager}>Next</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>

                    ) : (
                        <tr>
                            <td colSpan={3}>No contacts</td>
                        </tr>
                    )}

                </div>
                <AddContact addContact={addContact}/>
                <DeleteContact contactId={selectedContactId} deleteContact={deleteContact}/>
                <EditContact contact={currentContact} updateContact={updateContact}/>
            </div>
        </div>
    );


};
export default ContactList;