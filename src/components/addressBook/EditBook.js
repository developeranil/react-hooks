import React, {useEffect, useRef, useState} from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import CustomException from "../../common/CustomException";
import ModalPageLoading from "../loading/ModalPageLoading";

const EditBook = (props) => {
    const [currentBook, setCurrentBook] = useState(props.book);
    const [isLoading, setIsLoading] = useState(false);
    const name = useRef();
    const title = useRef();
    const address = useRef();
    const description = useRef();
    const date = useRef();

    useEffect(
        () => {
            setCurrentBook(props.book);
        },
        [props, setCurrentBook, currentBook]
    );


    EditBook.propTypes = {
        updateBook: PropTypes.func,
        currentBook: PropTypes.oneOfType([
            PropTypes.object,
            PropTypes.array

        ])
    };
    const handleSubmit = event => {
        setIsLoading(true);
        event.preventDefault();
        const book = {
            name: name.current.value,
            title: title.current.value,
            address: address.current.value,
            description: description.current.value,
            date: date.current.value,
        };
        const bookId = props.book.id;
        axios.patch('http://localhost/laravel-crud-api/api/public/api/stories/' + bookId, book).then(response => {
            if (response.data) {
                setIsLoading(false);
                props.updateBook(props.book, props.book.id);
                document.getElementById('btnCloseEditBookModal').click();
            }
        }).catch(error => {
            new CustomException('EditBook', error);
        });
    };

    return (
        <div className="modal fade" id="EditBookModal" tabIndex="-1" role="dialog"
             aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header bg-primary text-white">
                        <h5 className="modal-title" id="exampleModalLongTitle"><i className="fa fa-book mr-2"/>Edit
                            Story</h5>
                        <button type="button" id="btnCloseEditBookModal" className="close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <ModalPageLoading isLoading={isLoading}/>
                        <form onSubmit={handleSubmit}>
                            <div className="form-group">
                                <label htmlFor="title">Full Name:</label>
                                <input type="text" className="form-control" ref={name} name="name"
                                       defaultValue={currentBook.name}
                                       required/>
                            </div>
                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label htmlFor="title">title:</label>
                                    <input type="text" className="form-control" ref={title} name="author"
                                           defaultValue={currentBook.title}
                                           required/>
                                </div>
                                <div className="form-group col-md-6">
                                    <label htmlFor="title">Address:</label>
                                    <input type="text" className="form-control" ref={address} name="address"
                                           defaultValue={currentBook.address}
                                           required/>
                                </div>
                            </div>
                            <div className="form-group">
                                <label htmlFor="title">Date:</label>
                                <input type="date" className="form-control" ref={date} name="name"
                                       defaultValue={currentBook.date}
                                       required/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="title">Description:</label>
                                <input type="text" className="form-control" ref={description} name="description"
                                       defaultValue={currentBook.description}
                                />
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-outline-secondary" data-dismiss="modal"><i
                                    className="mr-1  fa fa-times"/>Close
                                </button>
                                <button type="submit" className="btn btn-primary"><i className="fa fa-check mr-2"/>Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default EditBook;