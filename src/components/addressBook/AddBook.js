import React, {useRef, useState} from 'react';
import PropTypes from 'prop-types';
import axios from "axios";
import CustomException from "../../common/CustomException";
import ModalPageLoading from "../loading/ModalPageLoading";

const AddBook = (props) => {
    const [isLoading, setIsLoading] = useState(false);
    const name = useRef();
    const title = useRef();
    const address = useRef();
    const description = useRef();
    const date = useRef();

    AddBook.propTypes = {
        addBook: PropTypes.func,
    };


    const onSubmit = (event) => {
        setIsLoading(true);
        event.preventDefault();
        const book = {
            name: name.current.value,
            title: title.current.value,
            address: address.current.value,
            description: description.current.value,
            date: date.current.value,
        };
        let baseUrl = 'http://localhost/laravel-crud-api/api/public/api/stories';
        axios.post(baseUrl, book).then(response => {
            if (response.data) {
                setIsLoading(false);
                props.addBook();
                //clearing an input field after form submit
                name.current.value = '';
                title.current.value = '';
                address.current.value = '';
                description.current.value = '';
                date.current.value = '';
                // close modal
                document.getElementById('btnCloseAddBookModal').click();
            } else {
                setIsLoading(false);
            }

        }).catch(error => {
            new CustomException('AddBook', error);
        });
    };

    return (
        <div className="modal fade" id="AddBookModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header bg-primary text-white">
                        <h5 className="modal-title" id="exampleModalLongTitle"><i className="fa fa-book mr-2"/>Add
                            Story</h5>
                        <button type="button" id="btnCloseAddBookModal" className="close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <ModalPageLoading isLoading={isLoading}/>
                        <form onSubmit={onSubmit}>
                            <div className="form-group">
                                <label htmlFor="title"> Full Name:</label>
                                <input type="text" ref={name} className="form-control" name="crawler_name"
                                       required/>
                            </div>
                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label htmlFor="title">Title:</label>
                                    <input type="text" ref={title} className="form-control" name="author"
                                           required/>
                                </div>
                                <div className="form-group col-md-6">
                                    <label htmlFor="title">Address:</label>
                                    <input type="text" ref={address} className="form-control" name="address"
                                           required/>
                                </div>
                            </div>

                            <div className="form-group">
                                <label htmlFor="title"> Date:</label>
                                <input type="date" ref={date} className="form-control" name="crawler_name"
                                       required/>
                            </div>

                            <div className="form-group">
                                <label htmlFor="exampleFormControlTextarea1">Description</label>
                                <textarea className="form-control" ref={description} id="exampleFormControlTextarea1"
                                          rows="3"></textarea>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-outline-secondary" data-dismiss="modal"><i
                                    className="mr-1  fa fa-times"/>Close
                                </button>
                                <button type="submit" className="btn btn-primary"><i className="fa fa-check mr-2"/>Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default AddBook;