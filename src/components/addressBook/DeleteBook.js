import React, {useState, useEffect} from 'react';
import axios from "axios";
import PropTypes from 'prop-types';
import CustomException from "../../common/CustomException";
import ModalPageLoading from "../loading/ModalPageLoading";

const DeleteBook = (props) => {

    const [bookId, setBookId] = useState(props.bookId);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        setBookId(props.bookId)
    }, [props, bookId, setBookId]);

    const handleClick = () => {
        setIsLoading(true);
        const BookId = props.bookId;
        axios.delete('http://localhost/laravel-crud-api/api/public/api/stories/' + BookId).then(response => {
            if (response.data) {
                setIsLoading(false);
                // passing bookId  to child to parent component
                props.deleteBook(props.bookId);
                // close modal
                document.getElementById('btnCloseBookDeleteModal').click();
            }
        }).catch(function (error) {
            new CustomException('DeleteBook', error);
        });
    };

    return (
        <section className="section-modal">
            <div className="modal fade" id="DeleteBookModal" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content position-relative">
                        <div className="modal-header bg-primary text-white">
                            <h5 className="modal-title" id="exampleModalLongTitle"><i className="fa fa-trash mr-2"/> Confirm Delete
                                </h5>
                            <button type="button" id="btnCloseBookDeleteModal" className="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body pb-0 position-relative">
                            <ModalPageLoading isLoading={isLoading}/>
                            <p className="text-bold"><strong>Are you sure?</strong></p>
                            <p className="text-muted">Do you really want to remove this story from your list?These process cannot be undo.</p>
                        </div>
                        <div className="modal-footer">
                            <button type="submit" className="btn btn-danger" onClick={handleClick}>
                                <i className="fa fa-trash mr-1"/>Remove
                            </button>
                            <button type="button" className="btn btn-outline-secondary" data-dismiss="modal"><i
                                className="fa fa-times mr-1"/> Cancel
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};
DeleteBook.propTypes = {
    deleteBook: PropTypes.func,
    bookId: PropTypes.number
};
export default DeleteBook;