import React, {useEffect, useRef, useState} from 'react';
import {Link} from "react-router-dom";
import AddBook from "./AddBook";
import DeleteBook from "./DeleteBook";
import EditBook from "./EditBook";
import axios from "axios";
import CustomException from "../../common/CustomException";
import PageLoading from "../loading/PageLoading";

const AddressBook = () => {
    const per_page = useRef();
    const search = useRef();
    const [books, setBooks] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const perPage = [5, 10, 20, 50];
    //pagination
    const [pageUrl, setPageUrl] = useState('http://localhost/laravel-crud-api/api/public/api/stories');
    const [first, setFirst] = useState('');
    const [last, setLast] = useState('');
    const [prev, setPrev] = useState('');
    const [next, setNext] = useState('');
    const [nextPage, setNextPage] = useState('');
    const [prevPage, setPrevPage] = useState('');
    const [currentPage, setCurrentPage] = useState('');
    const [lastPage, setLastPage] = useState('');
    const [to, setTo] = useState('');
    const [from, setFrom] = useState('');
    const [total, setTotal] = useState('');

    // perpage
    const [perpage] = useState(perPage);
    const [page, setPage] = useState('');

    // id
    const [selectedBookId, setSelectedBookId] = useState(null);

    const [currentBook, setCurrentBook] = useState({
        id: null,
        name: '',
        author: '',
        address: '',
        phone: '',
        description: ''
    });

    useEffect(() => {
        Book();
    }, [pageUrl, first, last, prevPage, lastPage, from]);


    const Book = () => {
        setIsLoading(true);
        let params = {
            keyword: search.current.value,
            per_page: per_page.current.value
        };
        let remoteUrl = (pageUrl == null) ? 'http://localhost/laravel-crud-api/api/public/api/stories' : pageUrl;
        axios.get(remoteUrl, {params: params}).then(response => {
            const book = response.data.links;
            const books = response.data.meta;
            if (response.data) {
                setIsLoading(false);
                setBooks(response.data.data || []);
                setCurrentPage(books.current_page);
                setLastPage(books.last_page);
                setFrom(books.form);
                setTo(books.to);
                setTotal(books.total);
                setFirst(book.first);
                setLast(book.last);
                setPrev(book.prev);
                setNext(book.next);
                setNextPage(books.current_page + 1);
                setPrevPage(books.current_page - 1);
            } else {
                setIsLoading(false);
            }
        }).catch(error => {
            new CustomException('AddressBook', error);
        });
    };
    // pagination
    const handlePager = (event) => {
        event.preventDefault();
        setPageUrl(event.target.href);
        Book();

    };

    //handlePage event pass
    const handlePage = event => {
        let selected = event.target.value;
        if (selected !== page) {
            setPage(event.target.value);
            Book();
        }
    };

    // add book
    function addBook() {
        setIsLoading(true);
        Book();
    }

    // delete book
    function deleteBook(bookId) {
        setIsLoading(true);
        setBooks(books.filter(book => book.id !== bookId));
        Book();
    }

    // update contact
    function updateBook(id, updateBook) {
        setIsLoading(true);
        setBooks(books.map(book => (book.id === id ? updateBook : book)));
        Book();
    }

    return (
        <div className="container">
            <section className="com-maincontent-header d-flex w-100">
                <div className="col-sm-4 text-right">
                    <ol className="breadcrumb breadcrumb-list">
                        <li className="breadcrumb-item"><Link to="/">Dashboard</Link>
                        </li>
                        <li className="breadcrumb-item active">Story
                        </li>
                    </ol>
                </div>
            </section>
            <div className="panel panel-default">
                <h1><i className="fa fa-book"/> Story Lists</h1>
                <div className="panel-heading">
                </div>
                <div className="panel-body ">
                    <div className="d-flex">
                        <h4>
                            <button type="submit" className=" btn btn-primary mr-2" data-toggle="modal"
                                    data-target="#AddBookModal"><i className="fa fa-plus mr-1"/>Add Story
                            </button>
                        </h4>
                        <h4><Link to="/" className="btn btn-primary mr-2 float-right"><i
                            className="fa fa-arrow-left"/> Back</Link></h4>
                    </div>
                    <div className="d-flex justify-content-between">
                        <div className="d-flex flex-row justify-content-center align-items-center">
                            <label className="mr-2">Perpage:</label>
                            <select ref={per_page}
                                    onChange={(event) => handlePage(event)}
                                    className="form-control">
                                {perpage.map((page, i) => {
                                    return <option key={i} defaultValue={page}>{page || 10}</option>;
                                })}
                            </select>
                        </div>
                        <div className="form-group d-flex flex-row float-right w-50">
                            <label htmlFor="search" className="mr-2 mt-2">Search:</label>
                            <input type="text" className="form-control search-input"
                                   ref={search} aria-describedby="search"
                                   onChange={Book} placeholder="search"/>
                        </div>
                    </div>
                    <PageLoading isLoading={isLoading}/>
                    {books.length > 0 ? (
                        <div>
                            <table className="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col"><i className="fa fa-star"/></th>
                                    <th scope="col">Name</th>
                                    <th scope="col">title</th>
                                    <th scope="col">Address</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">Date</th>
                                    <th scope="col" className="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                {books.map(book => (
                                    <tr key={book.id}>
                                        <td>{book.id}</td>
                                        <td><Link to={'stories/' + book.id}>{book.name}</Link></td>
                                        <td>{book.title}</td>
                                        <td>{book.address}</td>
                                        <td>{book.description ? book.description : 'N/A'}</td>
                                        <td>{book.created_at}</td>
                                        <td className="text-center d-flex">
                                            <Link to={'stories/' + book.id} className="btn btn-primary mr-2"><i
                                                className="fa fa-eye"/></Link>
                                            <button onClick={() => {
                                                setCurrentBook(book);
                                            }} className="btn btn-success mr-2" data-toggle="modal"
                                                    data-target="#EditBookModal"><i className="fa fa-pen"/></button>
                                            <button onClick={() => {
                                                setSelectedBookId(book.id);
                                            }} className="btn btn-danger" data-toggle="modal"
                                                    data-target="#DeleteBookModal"><i className="fa fa-times"/>
                                            </button>
                                        </td>
                                    </tr>
                                ))}

                                </tbody>
                            </table>
                            <div className=" d-flex justify-content-start"><label
                                className="mr-2">Page {currentPage} of {lastPage}</label>
                            </div>
                            <nav>
                                <ul className="pagination justify-content-end">
                                    <li className={prev === null ? 'page-item disabled' : 'page-item'}>
                                        <a className="page-link" href={prev} onClick={handlePager}
                                           tabIndex="-1">Previous</a>
                                    </li>
                                    <li className="page-item active">
                                        <a className="page-link" href={currentPage}
                                           onClick={handlePager}>{currentPage}<span
                                            className="sr-only">(current)</span></a>
                                    </li>
                                    <li className={to === total ? 'hide-element' : 'page-item'}><a
                                        className="page-link"
                                        onClick={handlePager}
                                        href={next}>{nextPage}</a>
                                    </li>
                                    <li className={next === null ? 'page-item disabled' : 'page-item'}>
                                        <a className="page-link" href={next} onClick={handlePager}>Next</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    ) : (
                        <tr>
                            <td colSpan={3}>No Stories</td>
                        </tr>
                    )}

                </div>
                <AddBook addBook={addBook}/>
                <EditBook book={currentBook} updateBook={updateBook}/>
                <DeleteBook bookId={selectedBookId} deleteBook={deleteBook}/>
            </div>
        </div>
    );

};
export default AddressBook;