import React, {useEffect, useState} from 'react';
import {Link} from "react-router-dom";
import axios from "axios";
import PropTypes from 'prop-types';
import CustomException from "../../common/CustomException";
import PageLoading from "../loading/PageLoading";

const ViewBook = (props) => {
    const [books, setBooks] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    ViewBook.propTypes = {
        match: PropTypes.shape({
            params: PropTypes.shape({
                id: PropTypes.number
            })
        })
    };

    useEffect(() => {
        let bookId = props.match.params.id;
        console.log(bookId);
        axios.get('http://localhost/laravel-crud-api/api/public/api/stories/' + bookId).then(response => {
            console.log(response);
            let books = response.data;
            if (response.data) {
                setIsLoading(false);
                setBooks(books || []);
            }
        }).catch(function (error) {
            new CustomException('ViewBook', error);
        });

    }, [setBooks, setIsLoading]);

    return (
        <div className="container">
            <div className="panel panel-default">
                <h1><i className="fa fa-book"></i>View Story</h1>
                <div className="panel-heading">
                </div>
                <PageLoading isLoading={isLoading}/>
                <div className="panel-body ">
                    <div className="d-flex">
                        <h4><Link to="/story" className="btn btn-primary mr-2 float-right"><i
                            className="fa fa-arrow-left"></i> Back</Link></h4>
                    </div>
                    <div className="table-responsive">
                        <table className="table table-hover">
                            <tbody>
                            <tr>
                                <th width="28%" scope="row" className="p-2"> Name:</th>
                                <td className="p-2">{books.name}</td>
                            </tr>
                            <tr>
                                <th scope="row" className="p-2">Title:</th>
                                <td className="p-2">{books.title}</td>
                            </tr>
                            <tr>
                                <th scope="row" className="p-2">Address:</th>
                                <td className="p-2">{books.address}</td>
                            </tr>
                            <tr>
                                <th scope="row" className="p-2">Description:</th>
                                <td className="p-2">{books.description ? books.description : 'n/a'}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default ViewBook;