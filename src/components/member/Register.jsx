import React, {useState} from 'react';
import PropTypes from 'prop-types';

const Register = (props) => {
  const initialFormState = {id: null, first_name: '', last_name: '', email: '', password: ''};
  // state       //setState
  const [user, setUser] = useState(initialFormState);

  Register.prototype = {
    history: PropTypes.obj({
      push: PropTypes.obj
    })
  };

  const onChange = event => {
    event.preventDefault();
    // event target
    const {first_name, last_name, email, password, value} = event.target;
    // update user name,email value in state
    setUser({...user, [first_name]: value, [last_name]: value, [email]: value, [password]: value});
  };


  const onSubmit = (event) => {
    event.preventDefault();
    if (!user.first_name || !user.last_name || !user.email || !user.password) return;
    // update state value
    localStorage.setItem('email', user.email);
    localStorage.setItem('password', user.password);
    setUser(initialFormState);
    user.id = user.length + 1;
    setUser([...user, user]);
    props.history.push('/login');
    //passing parent to child  using props

  };
  return (
    <div className="container">
      <div className="panel panel-default">
        <h1><i className="fa fa-user"></i> Register</h1>
        <div className="panel-heading">
        </div>
        <div className="panel-body ">
          <form onSubmit={onSubmit}>
            <div className="form-group">
              <label htmlFor="example">First Name</label>
              <input type="text" className="form-control" name="first_name" defaultValue={user.first_name}
                aria-describedby="emailHelp" onChange={onChange} placeholder="Enter first name"/>
            </div>
            <div className="form-group">
              <label htmlFor="eEmail">Last Name</label>
              <input type="text" className="form-control" name="last_name" defaultValue={user.last_name}
                aria-describedby="emailHelp" onChange={onChange} placeholder="Enter last name"/>
            </div>
            <div className="form-group">
              <label htmlFor="Email1">Email address</label>
              <input type="email" className="form-control" name="email" defaultValue={user.email}
                aria-describedby="emailHelp" onChange={onChange} placeholder="Enter email"/>
            </div>
            <div className="form-group">
              <label htmlFor="Password1">Password</label>
              <input type="password" className="form-control" name="password" defaultValue={user.password}
                onChange={onChange} placeholder="Password"/>
            </div>
            <button type="submit" className="btn btn-primary">Submit</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Register;