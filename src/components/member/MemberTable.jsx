import React, {useState, useEffect, useRef} from 'react';
import PageLoading from "../loading/PageLoading";
import {Link} from 'react-router-dom';
import axios from 'axios';
import moment from 'moment/moment';
import Pagination from "../pagination/Pagination";
import CustomException from "../../common/CustomException";
import MemberMessage from "./MemberMessage";

const MemberTable = () => {

    // crawlers
    const [members, setMembers] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [axiosMsg, setAxiosMsg] = useState('');
    const per_page = useRef();
    const search = useRef();

    //perpage
    const [pageUrl, setPageUrl] = useState('members');
    const [perpage, setPerpage] = useState(['10', '20', '50']);
    const [page, setPage] = useState('');

    //pagination
    const [currentPage, setCurrentPage] = useState('');
    const [prevPageUrl, setPrevPageUrl] = useState('');
    const [firstPageUrl, setFirstPageUrl] = useState('');
    const [lastPageUrl, setLastPageUrl] = useState('');
    const [nextPageUrl, setNextPageUrl] = useState('');
    const [nextPage, setNextpage] = useState('');
    const [lastPage, setLastPage] = useState('');
    const [prevPage, setPrevPage] = useState('');
    const [to, setTo] = useState('');
    const [total, setTotal] = useState('');


    useEffect(() => {
        searchMember();
    }, [setPerpage,firstPageUrl,lastPageUrl,prevPage]);


    //make selected
    const handlePage = (event) => {
        let selected = event.target.value;
        if (selected !== page) {
            setPage(event.target.value);
            searchMember();
        }
    };

    //paginate perPage
    const handlePager = (e) => {
        e.preventDefault();
        setPageUrl(e.target.href);
        searchMember();
    };


    const searchMember = () => {
        setIsLoading(true);
        let params = {
            keyword: search.current.value,
            per_page: per_page.current.value
        };
        let remoteUrl = (pageUrl === null) ? 'members' : pageUrl;
        axios.get(remoteUrl, {params: params}).then(response => {
            let members = response.data.members;
            let member = response.data;
            if (response.data.status) {
                //check response data
                setIsLoading(false);
                setMembers(members || []);
                setAxiosMsg(members.length ? '' : 'No members available');
                setCurrentPage(member.current_page);
                setPrevPageUrl(member.prev_page_url);
                setFirstPageUrl(member.first_page_url);
                setLastPageUrl(member.last_page_url);
                setNextPageUrl(member.next_page_url);
                setFirstPageUrl(member.first_page_url);
                setNextpage(member.current_page + 1);
                setPrevPage(member.current_page - 1);
                setTotal(member.total);
                setLastPage(member.last_page);
                setTo(member.to);
            } else {
                setIsLoading(false);
            }
        }).catch(function (error) {
            setIsLoading(false);
            new CustomException('MemberTable', error);
        });
    };


    return (
        <div>
            {/*<!--Loading Integration!-->*/}
            {/*<!--main-content!-->*/}
            <div className="wrapper-main-content">
                <div className="container main-content color-gray-light pt-4">
                    <section className="com-maincontent-header d-flex w-100">
                        <div className="col-sm-4 text-right">
                            <ol className="breadcrumb breadcrumb-list">
                                <li className="breadcrumb-item"><Link to="/">Home</Link>
                                </li>
                                <li className="breadcrumb-item active">Members List
                                </li>
                            </ol>
                        </div>
                    </section>
                    <section className="section-project-list mt-3">
                        <div className="card rounded-0 border-top-color">
                            <div className={members.length > 0 ? '' : 'hide-element'}>
                                <div className="card-header bg-white border-0 d-flex flex-column">
                                    <div className="mt-2 float-left">
                                        <i className="fa fa-users mr-1"/> Member lists
                                    </div>
                                    <div className="d-flex justify-content-between">
                                        <div className="d-flex flex-row justify-content-center align-items-center">
                                            <label className="mr-2">Perpage:</label>
                                            <select ref={per_page}
                                                    onChange={(event) => handlePage(event)}
                                                    className="form-control">
                                                {perpage.map((page, i) => {
                                                    return <option key={i} defaultValue={page}>{page || 10}</option>;
                                                })}
                                            </select>
                                        </div>
                                        <div className="form-group d-flex flex-row float-right w-50">
                                            <label htmlFor="search" className="mr-2 mt-2">Search:</label>
                                            <input type="text" className="form-control search-input"
                                                   ref={search} aria-describedby="search"
                                                   onChange={searchMember} placeholder="search"/>
                                        </div>
                                    </div>
                                </div>
                                <PageLoading isLoading={isLoading}/>
                                <div className="card-body pt-0">
                                    {/*<!--MemberTableList component--!>*/}
                                    <div className="table-responsive">
                                        {/*<!--Table-start--!>*/}
                                        <table className="table table-hover">
                                            <thead>
                                            <tr>
                                                <th width="50px"/>
                                                <th width="13%">First Name</th>
                                                <th width="13%">Last Name</th>
                                                <th width="13%">DOB</th>
                                                <th width="20%">Email</th>
                                                <th width="10%">Status</th>
                                                <th>Date</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {members.map((member, i) => {
                                                return <tr key={i}>
                                                    <td >
                                                        {++i}
                                                    </td>
                                                    <td>
                                                        {member.first_name}
                                                    </td>
                                                    <td>
                                                        {member.last_name}
                                                    </td>
                                                    <td>
                                                        {member.dob}
                                                    </td>
                                                    <td>
                                                        {member.email}
                                                    </td>
                                                    <td>
                                                        {member.is_active ?
                                                            <span className="badge badge-success">Active</span> :
                                                            <span className="badge badge-secondary">InActive</span>}
                                                    </td>
                                                    <td>
                                                        {moment(member.created_at).format(' MMMM Do, YYYY')}
                                                    </td>
                                                </tr>;
                                            })}
                                            </tbody>
                                        </table>
                                        {/*<!--Table-end--!>*/}
                                    </div>
                                    {/*<!--Pagination--!>*/}
                                    <Pagination handlePager={handlePager} currentPage={currentPage}
                                                lastPage={lastPage} prevPageUrl={prevPageUrl}
                                                to={to} total={total}
                                                nextPageUrl={nextPageUrl} nextPage={nextPage}/>
                                    {/*<!--Pagination-end--!>*/}
                                </div>
                            </div>
                            {/*<!--MemberMessage component--!>*/}
                            <MemberMessage members={members} isLoading={isLoading} axiosMsg={axiosMsg}/>
                        </div>
                    </section>
                </div>
            </div>
            {/*<!--main-content-close!-->*/}
        </div>
    );
};
export default MemberTable;