import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';

const ViewMember = (props) => {
  const userData = [
    {id: 1, name: 'Anil', email: 'anil@stha.com'},
    {id: 2, name: 'Ashesh', email: 'Ashesh@Adhikari.com'}];
  const [users] = useState(userData);

  ViewMember.propTypes = {
    match: PropTypes.shape({
      params: PropTypes.shape({
        id: PropTypes.number
      })
    })
  };

  const [user] = useState(() => {
    for (let user of users) {
      if (user.id === Number(props.match.params.id)) {
        return user;
      }
    }
    console.log(user);
  });

  return (
    <div className="container">
      <div className="panel panel-default">
        <h1><i className="fa fa-users"></i>View User</h1>
        <div className="panel-heading">
        </div>
        <div className="panel-body ">
          <div className="d-flex">
            <h4><Link to="/list" className="btn btn-primary mr-2 float-right"><i
              className="fa fa-arrow-left"></i> Back</Link></h4>
          </div>
          <div className="table-responsive">
            <table className="table table-hover">
              <tbody>
                <tr>
                  <th width="28%" scope="row" className="p-2"> Name:</th>
                  <td className="p-2">{user.name}</td>
                </tr>
                <tr>
                  <th scope="row" className="p-2">Email:</th>
                  <td className="p-2">{user.email}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ViewMember;