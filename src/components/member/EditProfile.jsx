import React, {useState, useEffect, useRef} from 'react';
import CustomException from "../../common/CustomException";
import axios from 'axios';
import ModalPageLoading from "../loading/ModalPageLoading";


const EditProfile = (props) => {
    const [currentMember, setCurrentMember] = useState(props.profile);
    const [isLoading, setIsLoading] = useState(false);
    const last_name = useRef();
    const first_name = useRef();
    const dob = useRef();

    useEffect(() => {
        setCurrentMember(props.profile);
    }, [props, setCurrentMember,currentMember]);


    const onSubmit = (e) => {
        e.preventDefault();
        setIsLoading(true);
        const data = {
            first_name: first_name.current.value,
            last_name: last_name.current.value,
            dob: dob.current.value
        };
        let uri = 'member/details';
        axios.patch(uri, data).then(response => {
            //check response data
            if (response.data.status === true) {
                setIsLoading(false);
                // passing props to parent to child component
                props.profile();
                // close modal
                document.getElementById('btnCloseProfileModal').click();
            } else {
                setIsLoading(false);
            }
        }).catch(error => {
            new CustomException('EditProfile', error);
        });
    };

    return (
        <section className="section-modal">
            {/*Modal-start*/}
            <div className="modal fade" id="editProfileModal" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        {/*Modal-header*/}
                        <div className="modal-header bg-primary text-white">
                            <h5 className="modal-title" id="exampleModalLongTitle"><i className="fa fa-pen mr-2"/>Edit
                                Profile</h5>
                            <button id="btnCloseProfileModal" type="button" className="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true" className="text-white">&times;</span>
                            </button>
                        </div>
                        {/* End Modal-header*/}
                        <div className="modal-body pb-0 position-relative">
                            <ModalPageLoading isLoading={isLoading}/>
                            {/*Loading integration*/}
                            {/*Form open*/}
                            <form onSubmit={onSubmit}>
                                {/*<!--First name--!>*/}
                                <div className="form-group">
                                    <label htmlFor="first_name"> First Name</label>
                                    <input type="text" ref={first_name} className="form-control" name="first_name"/>
                                </div>
                                {/*<!--Last name--!>*/}
                                <div className="form-group">
                                    <label htmlFor="last_name"> Last Name</label>
                                    <input type="text" ref={last_name} className="form-control" name="last_name"
                                          />
                                </div>
                                {/*<!--DOB--!>*/}
                                <div className="form-group">
                                    <label htmlFor="dob"> DOB</label>
                                    <input type="date" ref={dob} className="form-control" name="dob"
                                           />
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-outline-primary"
                                            data-dismiss="modal">Cancel
                                    </button>
                                    <button type="submit" className="btn btn-primary">Save</button>
                                </div>
                            </form>
                            {/*Form close*/}
                        </div>
                    </div>
                </div>
            </div>
            {/*Modal-close*/}
        </section>
    );
};
export default EditProfile;