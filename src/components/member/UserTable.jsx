import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import AddUser from './AddUser';
import DeleteUser from './DeleteUser';
import EditUser from './EditUser';

const UserTable = () => {
    const userData = [
        {id: 1, name: 'Anil', email: 'anil@stha.com'},
        {id: 2, name: 'suraj', email: 'Suraj@Thapa.com'}];
    //state //setState   //initialState value
    const [users, setUsers] = useState(userData);
    //state //setState   //initialState value
    const [selectedUserId, setSelectedUserId] = useState(null);
    //state //setState   //initialState value
    const [currentUser, setCurrentUser] = useState({id: null, name: '', email: ''});

    //add user
    function addUser(user) {
        user.id = users.length + 1;
        // update  user value to the state
        setUsers([...users, user]);
    }

    function editRow(user) {
        setCurrentUser({id: user.id, name: user.name, email: user.email});
    }


    //delete users
    function deleteUser(userId) {
        //this.setState(users.filter(user=>user.id)
        setUsers(users.filter(user => user.id !== userId));
    }


    function updateUser(id, updatedUser) {
        setUsers(users.map(user => (user.id === id ? updatedUser : user)));
    }

    return (
        <div className="container">
            <div className="panel panel-default">
                <h1><i className="fa fa-users"></i> UserLists</h1>
                <div className="panel-heading">
                </div>
                <div className="panel-body ">
                    <div className="d-flex">
                        <h4>
                            <button type="submit" className=" btn btn-primary mr-2" data-toggle="modal"
                                    data-target="#AddUserModal"><i className="fa fa-user mr-1"></i>Add User
                            </button>
                        </h4>
                        <h4><Link to="/" className="btn btn-primary mr-2 float-right"><i
                            className="fa fa-arrow-left"></i> Back</Link></h4>
                    </div>
                    {users.length > 0 ? (
                        <table className="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col"><i className="fa fa-star"></i></th>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col" className="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            {users.map(user => (
                                <tr key={user.id}>
                                    <td>{user.id}</td>
                                    <td><Link to={'member/' + user.id}>{user.name}</Link></td>
                                    <td width="20%">{user.email}</td>
                                    <td className="text-center">
                                        <Link to={'user/' + user.id} className="btn btn-primary mr-2"><i
                                            className="fa fa-eye"></i></Link>
                                        <button onClick={() => {
                                            editRow(user);
                                        }} className="btn btn-success mr-2" data-toggle="modal"
                                                data-target="#EditUserModal"><i className="fa fa-pen"></i></button>
                                        <button onClick={() => {
                                            setSelectedUserId(user.id);
                                        }} className="btn btn-danger" data-toggle="modal"
                                                data-target="#DeleteGroupModal"><i className="fa fa-times"></i></button>
                                    </td>
                                </tr>
                            ))}

                            </tbody>
                        </table>
                    ) : (
                        <tr>
                            <td colSpan={3}>No users</td>
                        </tr>
                    )}
                </div>
                <AddUser addUser={addUser}/>
                <DeleteUser userId={selectedUserId} deleteUser={deleteUser}/>
                <EditUser currentUser={currentUser} updateUser={updateUser}/>
            </div>
        </div>
    );
};

export default UserTable;