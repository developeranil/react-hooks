import React from 'react';
import moment from 'moment/moment';

const ProfileTable = (props) => {
    return (
        <div>
            <div className="table-responsive">
                <table className="table table-hover">
                    <tbody>
                    <tr>
                        <th width="28%" scope="row" className="p-2"> First Name:</th>
                        <td className="p-2">{props.member.first_name}</td>
                    </tr>
                    <tr>
                        <th scope="row" className="p-2"> Last Name:</th>
                        <td className="p-2">{props.member.last_name}</td>
                    </tr>
                    <tr>
                        <th scope="row" className="p-2">DOB:</th>
                        <td className="p-2">{moment(props.member.dob).format('MMMM Do, YYYY')}</td>
                    </tr>
                    <tr>
                        <th scope="row" className="p-2">Email:</th>
                        <td className="p-2">{props.member.email}</td>
                    </tr>
                    <tr>
                        <th scope="row" className="p-2">User:</th>
                        <td className="p-2">{props.member.is_active ? (
                            <span>Active</span>) : (<span>InActive</span>)}</td>
                    </tr>
                    <tr>
                        <th scope="row" className="p-2">Role:</th>
                        <td className="p-2">{props.member.is_admin ? (
                            <span>Admin</span>) : (<span>User</span>)}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    );
};
export default ProfileTable;
