import React from 'react';

const MemberMessage = (props) => (
    <div className={props.members.length > 0 ? 'hide-element' : ''}>
        <div className="container-fluid">
            <p className="mb-0 pb-4 border border-light pt-4 bg-white">
                {props.isLoading ? (<span>loading...</span>) : (<span>{props.axiosMsg}</span>)}
            </p>
        </div>
    </div>
);
export default MemberMessage;
