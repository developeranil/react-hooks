import React, {useState,useEffect} from 'react';
import PropTypes from 'prop-types';

const EditUser=(props)=>{
  const [ user, setUser ] = useState(props.currentUser);

  useEffect(
    () => {
      setUser(props.currentUser);
    },
    [ props ]
  );

  EditUser.propTypes = {
    updateUser: PropTypes.func,
    currentUser:PropTypes.obj
  };

  const handleSubmit=event=>{
    event.preventDefault();
    props.updateUser(user.id, user);
    document.getElementById('btnCloseEditUserModal').click();
  };
  const onChange = event => {
    // event target
    const {name, value,email} = event.target;
    // update user name,email value in state
    setUser({...user, [name]: value,[email]:value});
  };


  return(
    <div className="modal fade" id="EditUserModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header bg-primary text-white">
            <h5 className="modal-title" id="exampleModalLongTitle"><i className="fa fa-user mr-2"/>Edit User</h5>
            <button type="button" id="btnCloseEditUserModal" className="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <form onSubmit={handleSubmit}>
              <div className="form-group">
                <label htmlFor="title"> Name:</label>
                <input type="text" className="form-control" name="name" defaultValue={user.name}  onChange={onChange}  required/>
              </div>
              <div className="form-group">
                <label htmlFor="title">Email:</label>
                <input type="email" className="form-control" name="email" defaultValue={user.email} onChange={onChange}   required/>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-outline-secondary" data-dismiss="modal"><i className="mr-1  fa fa-times"/>Close</button>
                <button type="submit" className="btn btn-primary"><i className="fa fa-check mr-2"/>Save</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EditUser;