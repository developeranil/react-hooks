import React, {useRef, useState} from 'react';
import axios from 'axios';
import CustomException from "../../common/CustomException";
import ModalPageLoading from "../loading/ModalPageLoading";

const ChangeEmail = (props) => {
    const email = useRef();
    const password = useRef();

    const [isLoading, setIsLoading] = useState(false);

    const handleSubmit = (e) => {
        setIsLoading(true);
        e.preventDefault();
        const Email = {
            email: email.current.value,
            password: password.current.value
        };
        axios.patch('member/email', Email).then(response => {
            // check response data
            if (response.data.status) {
                setIsLoading(false);
                // console.log('response');
                // passing props to parent to child component
                props.email();
                //clearing an input field after form submit
                email.current.value = '';
                password.current.value = '';
                // close modal
                document.getElementById('btnCloseEmailModal').click();
            }
        }).catch(error => {
            new CustomException('ChangeEmail', error);
        });
    };

    return (
        <section className="section-modal">
            {/*<!--modal start--!>*/}
            <div className="modal fade" id="changeEmailModal" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header bg-primary text-white">
                            <h5 className="modal-title" id="exampleModalLongTitle"><i
                                className="fa fa-envelope mr-2"/>Change
                                Email</h5>
                            <button type="button" id="btnCloseEmailModal" className="close" data-dismiss="modal"
                                    aria-label="Close"><span aria-hidden="true" className="text-white">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body pb-0 position-relative">
                            <ModalPageLoading isLoading={isLoading}/>
                            <form onSubmit={handleSubmit}>
                                {/*<!--Email--!>*/}
                                <div className="form-group">
                                    <label htmlFor="email"> Email</label>
                                    <input type="email" ref={email} className="form-control" name="email"
                                           id="email" placeholder="abc@gmail.com"
                                           required/>
                                </div>
                                {/*<!--Password--!>*/}
                                <div className="form-group">
                                    <label htmlFor="new_password"> Password</label>
                                    <input type="password" ref={password} className="form-control" name="password"
                                           id="password" placeholder=" Password"
                                           required/>
                                </div>
                                {/*<!--modal-footer--!>*/}
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-outline-primary"
                                            data-dismiss="modal">Cancel
                                    </button>
                                    <button type="submit" className="btn btn-primary">Change Email</button>
                                </div>
                            </form>
                            {/*<!--form close--!>*/}
                        </div>
                    </div>
                </div>
            </div>
            {/*<!--modal end--!>*/}
        </section>
    );
};

export default ChangeEmail;