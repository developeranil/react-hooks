import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';

const AddUser = (props) => {
    const initialFormState = {id: null, name: '', email: ''};
    // state       //setState
    const [user, setUser] = useState(initialFormState);
    const [errors, setErrors] = useState({});
    const [isSubmitting, setIsSubmitting] = useState(false);

    useEffect(() => {
        if (Object.keys(errors).length === 0 && isSubmitting) {
        }
    }, [errors]);

    const onSubmit = (event) => {
        if (event) event.preventDefault();
        // update errors
        setErrors(validate(user));
        setIsSubmitting(true);
        if (!user.email || !user.name) return;
        // update state value
        setUser(initialFormState);
        //passing parent to child  using props
        props.addUser(user);
        // modal close
        document.getElementById('btnCloseAddUserModal').click();
    };

    //validation
    function validate(user) {
        let errors = {};
        let filter = /^\S+@\S+$/;
        if (!user.email) {
            errors.email = 'Email field is required';
        } else if (!filter.test(user.email)) {
            errors.email = 'Email  is invalid';
        }
        if (!user.name) {
            errors.name = 'Name field is required';
        }
        return errors;
    }

    // onchange event pass
    const onChange = event => {

        // event target
        const {name, value, email} = event.target;

        // update user name,email value in state
        setUser({...user, [name]: value, [email]: value});
    };

    return (
        <div className="modal fade" id="AddUserModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header bg-primary text-white">
                        <h5 className="modal-title" id="exampleModalLongTitle"><i className="fa fa-user mr-2"/>Add
                            User</h5>
                        <button type="button" id="btnCloseAddUserModal" className="close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <form onSubmit={onSubmit} noValidate>
                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label htmlFor="title"> Name:</label>
                                    <input type="text" className="form-control" name="name"
                                           value={user.name || ''}
                                           placeholder=" Name" onChange={onChange} required/>
                                    {errors.name && (
                                        <p className="text-danger">{errors.name}</p>
                                    )}
                                </div>
                                <div className="form-group col-md-6">
                                    <label htmlFor="title">Email:</label>
                                    <input type="email" className="form-control" name="email"
                                           value={user.email || ''}
                                           placeholder="Email" onChange={onChange} required/>
                                    {errors.email && (
                                        <p className="text-danger">{errors.email}</p>
                                    )}
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-outline-secondary" data-dismiss="modal"><i
                                    className="mr-1  fa fa-times"/>Close
                                </button>
                                <button type="submit" className="btn btn-primary"><i className="fa fa-check mr-1"/>Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};
AddUser.propTypes = {
    addUser: PropTypes.func,
};
export default AddUser;
