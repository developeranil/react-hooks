import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';
import PageLoading from "../loading/PageLoading";
import CustomException from "../../common/CustomException";
import ProfileTable from "./ProfileTable";
import EditProfile from "./EditProfile";
import ChangePassword from "./ChangePassword";
import ChangeEmail from "./ChangeEmail";

const Profile = () => {
    const [member, setMember] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        profileData();
    }, [setMember, setIsLoading]);

    const profileData = () => {
        axios.get('member').then(response => {
            let member = response.data.member;
            // check response
            if (response.data.status) {
                setIsLoading(false);
                setMember(member || {})
            }
        }).catch(function (error) {
            new CustomException('Profile', error);
        });
    };

    const editProfile = () => {
        profileData();
    };

    const changePassword = () => {
        profileData();
    };

    const changeEmail = () => {
        setIsLoading(true);
        profileData();
    };

    return (
        <div>
            <div className="wrapper-main-content">
                <div className="container main-content color-gray-light pt-4">
                    <section className="com-maincontent-header d-flex w-100">
                        <div className="col-sm-4 text-right">
                            <ol className="breadcrumb breadcrumb-list">
                                <li className="breadcrumb-item"><Link to="/">Home</Link>
                                </li>
                                <li className="breadcrumb-item active">Profile
                                </li>
                            </ol>
                        </div>
                    </section>
                    <section className="section-project-list mt-3">
                        <div className="card rounded-0 border-top-color">
                            <div className={member ? '' : 'hide-element'}>
                                <div className="card-header bg-white border-0">
                                    <div className="mt-2 float-left">
                                        Profile Information
                                    </div>
                                    <button type="button" className="btn btn-primary py-2 px-3 mx-0 float-right"
                                            data-toggle="modal" data-target="#editProfileModal">
                                        <span>Edit Profile</span></button>
                                </div>

                                <div className="card-body pt-0">
                                    <PageLoading isLoading={isLoading}/>
                                    <ProfileTable member={member}/>
                                </div>
                            </div>
                            <div className="card-footer com-card-footer">
                                <button type="button" className="btn btn-danger py-2 px-3 mx-0 float-right mr-2"
                                        data-toggle="modal" data-target="#changePasswordModal">
                                    <span>Change Password</span></button>
                                <button type="button" className="btn btn-primary py-2 px-3 mx-0 float-right mr-2"
                                        data-toggle="modal" data-target="#changeEmailModal"><span>Change Email</span>
                                </button>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <ChangeEmail email={changeEmail}/>
            <ChangePassword password={changePassword}/>
            <EditProfile profile={editProfile}/>
        </div>
    );
};
export default Profile;