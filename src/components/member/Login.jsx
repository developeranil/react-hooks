import React, {useEffect, useRef} from 'react';
import axios from 'axios';
import CustomException from "../../common/CustomException";

const Login = (props) => {
    const password = useRef();
    const email = useRef();
    useEffect(() => {
        if (localStorage.getItem('X-Auth-Token')) {
            props.history.push('/');
        }
        axios.get('validate-token').then(response => {
            if (response.data.status) {
                props.history.push('/');
            } else {
                props.history.push('/member/login');
            }
        }).catch(e => {
            new CustomException('Login', e);
        });
    }, []);

    const onSubmit = (e) => {
        e.preventDefault();
        const login = {
            email: email.value,
            password: password.value
        };
        let url = 'login';
        axios.post(url, login).then(response => {
            // check response
            if (response.data.status === true) {
                props.history.push('/');
            } else {
                this.setState({
                    isLogged: true,
                    isLoading: false
                });
            }
        }).catch(error => {
            this.setState({
                isLoading: false
            });
            new CustomException('Login', error);
        });
    };
    return (
        <div className="container">
            <div className="panel panel-default">
                <h1><i className="fa fa-user"/> Login</h1>
                <div className="panel-heading">
                </div>
                <div className="panel-body ">
                    <form onSubmit={onSubmit}>
                        <div className="form-group">
                            <label htmlFor="Email1">Email address</label>
                            <input type="email" className="form-control" name="email" ref={email}
                                   aria-describedby="emailHelp" placeholder="Enter email"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="Password1">Password</label>
                            <input type="password" className="form-control" name="password"
                                   ref={password} placeholder="Password"/>
                        </div>
                        <button type="submit" className="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default Login;