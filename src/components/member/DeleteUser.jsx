import React from 'react';
import PropTypes from 'prop-types';

const DeleteUser = (props) => {

  const handleClick = () => {
    props.deleteUser(props.userId);
    // close modal
    document.getElementById('btnCloseGroupDeleteModal').click();
  };

  return (
    <section className="section-modal">
      <div className="modal fade" id="DeleteGroupModal" tabIndex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content position-relative">
            <div className="modal-header bg-primary text-white">
              <h5 className="modal-title" id="exampleModalLongTitle"><i className="fa fa-trash mr-2"/>Delete
                                User</h5>
              <button type="button" id="btnCloseGroupDeleteModal" className="close" data-dismiss="modal"
                aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body pb-0 position-relative">
              <p className="text-bold"><strong>Are you sure?</strong></p>
              <p className="text-muted">Do you really want to delete these User?</p>
            </div>
            <div className="modal-footer">
              <button type="submit" className="btn btn-danger" onClick={handleClick}>
                <i className="fa fa-trash mr-1"/>Delete
              </button>
              <button type="button" className="btn btn-outline-secondary" data-dismiss="modal"><i className="fa fa-times mr-1"/> Cancel</button>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};
DeleteUser.propTypes = {
  deleteUser: PropTypes.func,
  userId:PropTypes.number
};
export default DeleteUser;