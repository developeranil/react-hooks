import React, {useRef, useState} from 'react';
import axios from 'axios';
import CustomException from "../../common/CustomException";
import ModalPageLoading from "../loading/ModalPageLoading";

const ChangePassword = (props) => {
    const old_password = useRef();
    const new_password = useRef();
    const confirm_password = useRef();

    const [isLoading, setIsLoading] = useState(false);

    const handleSubmit = (event) => {
        event.preventDefault();
        setIsLoading(true);
        let newPassword = new_password.current.value || '';
        let confirmPassword = confirm_password.current.value || '';
        const Password = {
            old_password: old_password.current.value,
            new_password: newPassword,
            confirm_password: confirmPassword
        };
        let uri = 'member/password';
        axios.patch(uri, Password).then(response => {
            // check response data
            if (response.data.status === true) {
                setIsLoading(false);
                // passing props to parent to child component
                props.password();
                //clearing an input field after form submit
                old_password.current.value = '';
                new_password.current.value = '';
                confirm_password.current.value = '';
                // close modal
                document.getElementById('btnClosePasswordModal').click();
            }
        }).catch(error => {
            new CustomException('ChangePassword', error);
        });
    };

    return (
        <section className="section-modal">
            {/*<!--modal start--!>*/}
            <div className="modal fade" id="changePasswordModal" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header bg-primary text-white">
                            <h5 className="modal-title" id="exampleModalLongTitle"><i className="fa fa-lock mr-2"/>Change
                                Password</h5>
                            <button type="button" id="btnClosePasswordModal" className="close" data-dismiss="modal"
                                    aria-label="Close"><span aria-hidden="true" className="text-white">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body pb-0 position-relative">
                            <ModalPageLoading isLoading={isLoading}/>
                            <form onSubmit={handleSubmit}>
                                {/*<!--OldPassword--!>*/}
                                <div className="form-group">
                                    <label htmlFor="old_password"> Old Password</label>
                                    <input type="password" ref={old_password} className="form-control"
                                           name="old_password"
                                           id="old_password" placeholder=" Old Password" required/>
                                </div>
                                {/*<!--NewPassword--!>*/}
                                <div className="form-group">
                                    <label htmlFor="new_password">New Password</label>
                                    <input type="password" ref={new_password} className="form-control"
                                           name="new_password"
                                           id="new_password" placeholder=" New Password" required/>
                                </div>
                                {/*<!--ConfirmPassword--!>*/}
                                <div className="form-group">
                                    <label htmlFor="confirm_password"> Confirm Password</label>
                                    <input type="password" ref={confirm_password} className="form-control"
                                           name="confirm_password"
                                           id="confirm_password" placeholder=" Confirm Password" required/>
                                </div>
                                {/*<!--modal-footer--!>*/}
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-outline-primary"
                                            data-dismiss="modal">Cancel
                                    </button>
                                    <button type="submit" className="btn btn-primary">Change Password</button>
                                </div>
                            </form>
                            {/*<!--form close--!>*/}
                        </div>
                    </div>
                </div>
            </div>
            {/*<!--modal end--!>*/}
        </section>
    );
};
export default ChangePassword;