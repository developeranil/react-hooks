import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import axios from "axios";
import PropTypes from 'prop-types';
import CustomException from "../../common/CustomException";
import PageLoading from "../loading/PageLoading";

const ViewProject = (props) => {

    const [projects, setProjects] = useState([]);
    const [isLoading, setIsLoading] = useState(true);


    useEffect(() => {
        let projectId = props.match.params.id;
        const baseUrl = 'http://localhost/laravel-crud-api/api/public/api/projects/';
        axios.get(baseUrl + projectId).then(response => {
            let projects = response.data;
            if (response.data) {
                setIsLoading(false);
                setProjects(projects || [])
            } else {
                setIsLoading(false);
            }

        }).catch(errors => {
            new CustomException('ViewProject', errors)
        });
    }, []);

    ViewProject.propTypes = {
        match: PropTypes.shape({
            params: PropTypes.shape({
                id: PropTypes.number
            })
        })
    };

    return (
        <div className="container">
            <div className="panel panel-default">
                <h1><i className="fa fa-users"/>View Project</h1>
                <div className="panel-heading">
                </div>
                <PageLoading isLoading={isLoading}/>
                <div className="panel-body ">
                    <div className="d-flex">
                        <h4><Link to="/project" className="btn btn-primary mr-2 float-right"><i
                            className="fa fa-arrow-left"/> Back</Link></h4>
                    </div>
                    <div className="table-responsive">
                        <table className="table table-hover">
                            <tbody>
                            <tr>
                                <th width="28%" scope="row" className="p-2"> Project Name:</th>
                                <td className="p-2">{projects.name}</td>
                            </tr>
                            <tr>
                                <th scope="row" className="p-2">URL:</th>
                                <td className="p-2">{projects.url}</td>
                            </tr>
                            <tr>
                                <th scope="row" className="p-2">description:</th>
                                <td className="p-2">{projects.description ? projects.description : 'N/A'}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ViewProject;