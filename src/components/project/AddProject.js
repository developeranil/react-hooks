import React, {useState, useRef} from 'react';
import PropTypes from 'prop-types';
import axios from "axios";
import ModalPageLoading from "../loading/ModalPageLoading";
import CustomException from "../../common/CustomException";

const AddProject = (props) => {
    const [isLoading, setIsLoading] = useState(false);

    const name = useRef();
    const url = useRef();
    const description = useRef();


    const onSubmit = (event) => {
        setIsLoading(true);
        event.preventDefault();
        const project = {
            name: name.current.value,
            url: url.current.value,
            description: description.current.value,
        };
        let baseUrl = 'http://localhost/laravel-crud-api/api/public/api/projects';
        axios.post(baseUrl, project).then(response => {
            if (response.data) {
                setIsLoading(false);
                props.addProject();
                name.current.value = '';
                url.current.value = '';
                description.current.value = '';
                // close modal
                document.getElementById('btnCloseAddProjectModal').click();
            }
        }).catch(errors => {
             new CustomException('AddProject', errors)
        });
    };


    AddProject.propTypes = {
        addProject: PropTypes.func,
    };
    return (
        <div className="modal fade" id="AddProjectModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header bg-primary text-white">
                        <h5 className="modal-title" id="exampleModalLongTitle"><i className="fa fa-tasks mr-2"/>Add
                            Project</h5>
                        <button type="button" id="btnCloseAddProjectModal" className="close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <ModalPageLoading isLoading={isLoading}/>
                        <form onSubmit={onSubmit} noValidate>
                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label htmlFor="title"> Name:</label>
                                    <input type="text" className="form-control" name="name"
                                           placeholder=" name" ref={name} required/>
                                </div>
                                <div className="form-group col-md-6">
                                    <label htmlFor="title">URL:</label>
                                    <input type="text" className="form-control" name="url"
                                           placeholder="url" ref={url} required/>
                                </div>
                            </div>
                            <div className="form-group">
                                <label htmlFor="exampleFormControlTextarea1">Description</label>
                                <textarea className="form-control" ref={description} id="exampleFormControlTextarea1" rows="3"></textarea>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-outline-secondary" data-dismiss="modal"><i
                                    className="mr-1  fa fa-times"/>Close
                                </button>
                                <button type="submit" className="btn btn-primary"><i className="fa fa-check mr-2"/>Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default AddProject;