import React, {useState, useEffect, useRef} from 'react';
import PropTypes from 'prop-types';
import axios from "axios";
import CustomException from "../../common/CustomException";
import ModalPageLoading from "../loading/ModalPageLoading";

const EditProject = (props) => {
    const [currentProject, setCurrentProject] = useState(props.project);
    const [isLoading, setIsLoading] = useState(false);
    const name = useRef();
    const url = useRef();
    const description = useRef();

    // componentDidMount
    useEffect(
        () => {
            setCurrentProject(props.project);
        },
        [props, currentProject,]
    );

    EditProject.propTypes = {
        updateProject: PropTypes.func,
        currentProject: PropTypes.object
    };

    const handleSubmit = event => {
        setIsLoading(true);
        const project = {
            name: name.current.value,
            url: url.current.value,
            description: description.current.value,
        };
        event.preventDefault();
        const projectId = props.project.id;
        axios.patch('http://localhost/laravel-crud-api/api/public/api/projects/' + projectId, project).then(response => {
            if (response.data) {
                setIsLoading(false);
                props.updateProject(props.project.id, props.project);
                document.getElementById('btnCloseEditProjectModal').click();
            }
        }).catch(errors => {
            new CustomException('EditProject', errors)
        });
    };

    return (
        <div className="modal fade" id="EditProjectModal" tabIndex="-1" role="dialog"
             aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header bg-primary text-white">
                        <h5 className="modal-title" id="exampleModalLongTitle"><i className="fa fa-user mr-2"/>Edit
                            Project</h5>
                        <button type="button" id="btnCloseEditProjectModal" className="close"
                                data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <ModalPageLoading isLoading={isLoading}/>
                        <form onSubmit={handleSubmit}>
                            <div className="form-group">
                                <label htmlFor="title"> Name:</label>
                                <input type="text" ref={name} className="form-control" id="name"
                                       name="name"
                                       defaultValue={currentProject.name}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="title">Url:</label>
                                <input type="text" className="form-control" id="url" name="url"
                                       defaultValue={currentProject.url}
                                       ref={url}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="title">Description:</label>
                                <input type="text" className="form-control" id="description" name="description"
                                       defaultValue={currentProject.description}
                                       ref={description}/>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-outline-secondary"
                                        data-dismiss="modal"><i
                                    className="mr-1  fa fa-times"/>Close
                                </button>
                                <button type="submit" className="btn btn-primary"><i className="fa fa-check mr-2"/>Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default EditProject;