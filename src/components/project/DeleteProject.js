import React, {useState, useEffect} from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import ModalPageLoading from "../loading/ModalPageLoading";
import CustomException from "../../common/CustomException";

const DeleteProject = (props) => {
    const [projectId, setProjectId] = useState(props.projectId);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        setProjectId(props.projectId)
    }, [projectId, setProjectId, setIsLoading],);

    const handleClick = () => {
        setIsLoading(true);
        const projectId = props.projectId;
        axios.delete('http://localhost/laravel-crud-api/api/public/api/projects/' + projectId).then(response => {
            if (response.data) {
                setIsLoading(false);
                props.deleteProject(props.projectId);
                document.getElementById('btnCloseProjectDeleteModal').click();
            }
        }).catch(errors => {
            new CustomException('DeleteProject', errors)
        });
    };

    DeleteProject.propTypes = {
        deleteProject: PropTypes.func,
        projectId: PropTypes.number
    };

    return (
        <section className="section-modal">
            <div className="modal fade" id="DeleteProjectModal" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content position-relative">
                        <div className="modal-header bg-primary text-white">
                            <h5 className="modal-title" id="exampleModalLongTitle"><i className="fa fa-trash mr-2"/>Delete
                                Project</h5>
                            <button type="button" id="btnCloseProjectDeleteModal" className="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body pb-0 position-relative">
                            <ModalPageLoading isLoading={isLoading}/>
                            <p className="text-bold"><strong>Are you sure?</strong></p>
                            <p className="text-muted">Do you really want to remove this project?</p>
                        </div>
                        <div className="modal-footer">
                            <button type="submit" className="btn btn-danger" onClick={handleClick}>
                                <i className="fa fa-trash mr-1"/>Remove
                            </button>
                            <button type="button" className="btn btn-outline-secondary" data-dismiss="modal"><i
                                className="fa fa-times mr-1"/> Cancel
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};
export default DeleteProject;