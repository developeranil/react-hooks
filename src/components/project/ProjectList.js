import React, {useEffect, useRef, useState} from 'react';
import {Link} from 'react-router-dom';
import axios from "axios";
import AddProject from './AddProject';
import DeleteProject from './DeleteProject';
import EditProject from './EditProject';
import CustomException from "../../common/CustomException";
import PageLoading from "../loading/PageLoading";

const ProjectList = () => {

    //state    //setState
    const [projects, setProjects] = useState([]);

    const per_page = useRef();
    const search = useRef();

    //perpage
    const Page = [5, 10, 20, 50];
    const [perpage] = useState(Page);
    const [isLoading, setIsLoading] = useState(true);
    const [page, setPage] = useState('');

    // pagination
    const [pageUrl, setPageUrl] = useState('http://localhost/laravel-crud-api/api/public/api/projects');
    const [first, setFirst] = useState('');
    const [last, setLast] = useState('');
    const [prev, setPrev] = useState('');
    const [next, setNext] = useState('');
    const [nextPage, setNextPage] = useState('');
    const [prevPage, setPrevPage] = useState('');
    const [currentPage, setCurrentPage] = useState('');
    const [lastPage, setLastPage] = useState('');
    const [to, setTo] = useState('');
    const [from, setFrom] = useState('');
    const [total, setTotal] = useState('');


    //set projectId in state
    const [selectedProjectId, setSelectedProjectId] = useState(null);
    //set currentProject in state
    const [currentProject, setCurrentProject] = useState({id: null, name: '', url: ''});


    useEffect(() => {
        Project();
    }, [pageUrl, first, last, prevPage, from, setPageUrl]);


    //handle pagination
    const handlePager = event => {
        event.preventDefault();
        setPageUrl(event.target.href);
        Project();
    };

    //handlePage
    const handlePage = (event) => {
        event.preventDefault();
        let selected = event.target.value;
        if (selected !== page) {
            setPage(event.target.value);
            Project();
        }
    };
    const Project = () => {
        setIsLoading(true);
        let params = {
            keyword: search.current.value,
            per_page: per_page.current.value,
        };
        const baseUrl = (pageUrl == null) ? 'http://localhost/laravel-crud-api/api/public/api/projects' : pageUrl;
        axios.get(baseUrl, {params: params}).then(response => {
            const project = response.data.links;
            const projects = response.data.meta;
            if (response.data) {
                setIsLoading(false);
                setProjects(response.data.data || []);
                setCurrentPage(projects.current_page);
                setLastPage(projects.last_page);
                setFrom(projects.form);
                setTo(projects.to);
                setTotal(projects.total);
                setFirst(project.first);
                setLast(project.last);
                setPrev(project.prev);
                setNext(project.next);
                setNextPage(projects.current_page + 1);
                setPrevPage(projects.current_page - 1);

            } else {
                setIsLoading(false);
            }
        }).catch(errors => {
            new CustomException('ProjectList', errors)
        })
    };

    //add project
    function addProject(project) {
        setIsLoading(true);
        Project();
    }

    //delete users
    function deleteProject(projectId) {
        setIsLoading(true);
        setProjects(projects.filter(project => project.id !== projectId));
        Project();
    }

    function updateProject(id, updatedProject) {
        setIsLoading(true);
        setProjects(projects.map(project => (project.id === id ? updatedProject : project)));
        Project();
    }

    return (
        <div className="container">
            <div className="panel panel-default">
                <h1><i className="fa fa-tasks"/> ProjectLists</h1>
                <div className="panel-heading">
                </div>
                <PageLoading isLoading={isLoading}/>
                <div className="panel-body ">
                    <div className="d-flex">
                        <h4>
                            <button type="submit" className=" btn btn-primary mr-2" data-toggle="modal"
                                    data-target="#AddProjectModal"><i className="fa fa-tasks mr-1"/>Add Project
                            </button>
                        </h4>
                        <h4><Link to="/dashboard" className="btn btn-primary mr-2 float-right"><i
                            className="fa fa-arrow-left"/> Back</Link></h4>
                    </div>
                    <div className="d-flex justify-content-between">
                        <div className="d-flex flex-row justify-content-center align-items-center">
                        <label className="mr-2">Perpage:</label>
                        <select ref={per_page}
                                onChange={(event) => handlePage(event)}
                                className="form-control">
                            {perpage.map((page, i) => {
                                return <option key={i} defaultValue={page}>{page || 10}</option>;
                            })}
                        </select>
                    </div>
                        <div className="form-group d-flex flex-row float-right w-50">
                            <label htmlFor="search" className="mr-2 mt-2">Search:</label>
                            <input type="text" className="form-control search-input"
                                   ref={search} aria-describedby="search"
                                   onChange={Project} placeholder="search"/>
                        </div>
                    </div>
                    <div>
                        {projects.length > 0 ? (
                            <div>
                                <table className="table table-striped">
                                    <thead>
                                    <tr>
                                        <th scope="col"><i className="fa fa-star"/></th>
                                        <th scope="col">ProjectName</th>
                                        <th scope="col">URL</th>
                                        <th scope="col">Description</th>
                                        <th scope="col" className="text-center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {projects.map(project => (
                                        <tr key={project.id}>
                                            <td>{project.id}</td>
                                            <td><Link to={'view/' + project.id}>{project.name}</Link></td>
                                            <td>{project.url}</td>
                                            <td>{project.description ? project.description : 'N/A'}</td>
                                            <td className="text-center">
                                                <Link to={'view/' + project.id} className="btn btn-primary mr-2"><i
                                                    className="fa fa-eye"/></Link>
                                                <button onClick={() => {
                                                    setCurrentProject(project);
                                                }} className="btn btn-success mr-2" data-toggle="modal"
                                                        data-target="#EditProjectModal"><i className="fa fa-pen"/>
                                                </button>
                                                <button onClick={() => {
                                                    setSelectedProjectId(project.id);
                                                }} className="btn btn-danger" data-toggle="modal"
                                                        data-target="#DeleteProjectModal"><i className="fa fa-times"/>
                                                </button>
                                            </td>
                                        </tr>
                                    ))}
                                    </tbody>
                                </table>
                                <div className=" d-flex justify-content-start"><label
                                    className="mr-2">Page {currentPage} of {lastPage}</label>
                                </div>
                                <nav>
                                    <ul className="pagination justify-content-end">
                                        <li className={prev === null ? 'page-item disabled' : 'page-item'}>
                                            <a className="page-link" href={prev} onClick={handlePager}
                                               tabIndex="-1">Previous</a>
                                        </li>
                                        <li className="page-item active">
                                            <a className="page-link" href={currentPage}
                                               onClick={handlePager}>{currentPage}<span
                                                className="sr-only">(current)</span></a>
                                        </li>
                                        <li className={to === total ? 'hide-element' : 'page-item'}><a
                                            className="page-link"
                                            onClick={handlePager}
                                            href={next}>{nextPage}</a>
                                        </li>
                                        <li className={next === null ? 'page-item disabled' : 'page-item'}>
                                            <a className="page-link" href={next} onClick={handlePager}>Next</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        ) : (
                            <tr>
                                <td colSpan={3}>No Projects</td>
                            </tr>
                        )}
                    </div>
                </div>
                <AddProject addProject={addProject}/>
                <DeleteProject projectId={selectedProjectId} deleteProject={deleteProject}/>
                <EditProject project={currentProject} updateProject={updateProject}/>
            </div>
        </div>
    );
};

export default ProjectList;