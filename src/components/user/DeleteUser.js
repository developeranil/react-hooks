import React, {useState, useEffect} from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import ModalPageLoading from "../loading/ModalPageLoading";
import CustomException from "../../common/CustomException";

const DeleteUser = (props) => {
    const [userId, setUserId] = useState(props.userId);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        setUserId(props.userId)
    }, [userId, setUserId, setIsLoading],);

    const handleClick = () => {
        setIsLoading(true);
        const userId = props.userId;
        axios.delete('http://localhost/laravel-crud-api/api/public/api/members/' + userId).then(response => {
            if (response.data) {
                setIsLoading(false);
                props.deleteUser(props.userId);
                document.getElementById('btnCloseUserDeleteModal').click();
            }
        }).catch(errors => {
            new CustomException('DeleteUser', errors)
        });
    };

    DeleteUser.propTypes = {
        deleteUser: PropTypes.func,
        userId: PropTypes.number
    };

    return (
        <section className="section-modal">
            <div className="modal fade" id="DeleteUserModal" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content position-relative">
                        <div className="modal-header bg-primary text-white">
                            <h5 className="modal-title" id="exampleModalLongTitle"><i className="fa fa-user-times mr-2"/>Delete
                                User</h5>
                            <button type="button" id="btnCloseUserDeleteModal" className="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body pb-0 position-relative">
                            <ModalPageLoading isLoading={isLoading}/>
                            <p className="text-bold"><strong>Are you sure?</strong></p>
                            <p className="text-muted">Do you really want to remove this user?</p>
                        </div>
                        <div className="modal-footer">
                            <button type="submit" className="btn btn-danger" onClick={handleClick}>
                                <i className="fa fa-trash mr-1"/>Remove
                            </button>
                            <button type="button" className="btn btn-outline-secondary" data-dismiss="modal"><i
                                className="fa fa-times mr-1"/> Cancel
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};
export default DeleteUser;