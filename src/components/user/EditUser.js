import React, {useState, useEffect, useRef} from 'react';
import PropTypes from 'prop-types';
import axios from "axios";
import CustomException from "../../common/CustomException";
import ModalPageLoading from "../loading/ModalPageLoading";

const EditUser = (props) => {
    const [currentUser, setCurrentUser] = useState(props.user);
    const [isLoading, setIsLoading] = useState(false);
    const name = useRef();
    const email = useRef();

    // componentDidMount
    useEffect(
        () => {
            setCurrentUser(props.user);
        },
        [props, currentUser,]
    );

    EditUser.propTypes = {
        updateUser: PropTypes.func,
        currentUser: PropTypes.object
    };

    const handleSubmit = event => {
        setIsLoading(true);
        const user = {
            name: name.current.value,
            email: email.current.value
        };
        event.preventDefault();
        const userId = props.user.id;
        axios.patch('http://localhost/laravel-crud-api/api/public/api/members/' + userId, user).then(response => {
            if (response.data) {
                setIsLoading(false);
                props.updateUser(props.user.id, props.user);
                document.getElementById('btnCloseEditUserModal').click();
            }
        }).catch(errors => {
            new CustomException('EditUser', errors)
        });
    };

    return (
        <div className="modal fade" id="EditUserModal" tabIndex="-1" role="dialog"
             aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header bg-primary text-white">
                        <h5 className="modal-title" id="exampleModalLongTitle"><i className="fa fa-user mr-2"/>Edit
                            Project</h5>
                        <button type="button" id="btnCloseEditUserModal" className="close"
                                data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <ModalPageLoading isLoading={isLoading}/>
                        <form onSubmit={handleSubmit}>
                            <div className="form-group">
                                <label htmlFor="title"> Name:</label>
                                <input type="text" ref={name} className="form-control" id="name"
                                       name="name"
                                       defaultValue={currentProject.name}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="title">Url:</label>
                                <input type="text" className="form-control" id="url" name="url"
                                       defaultValue={currentProject.url}
                                       ref={url}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="title">Description:</label>
                                <input type="text" className="form-control" id="description" name="description"
                                       defaultValue={currentProject.description}
                                       ref={description}/>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-outline-secondary"
                                        data-dismiss="modal"><i
                                    className="mr-1  fa fa-times"/>Close
                                </button>
                                <button type="submit" className="btn btn-primary"><i className="fa fa-check mr-2"/>Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default EditUser;