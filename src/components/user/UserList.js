import React, {useEffect, useRef, useState} from 'react';
import {Link} from 'react-router-dom';
import axios from "axios";
import CustomException from "../../common/CustomException";
import PageLoading from "../loading/PageLoading";
import AddUser from "../user/AddUser";
import DeleteUser from "./DeleteUser";

const UserList = () => {

    //state    //setState
    const [users, setUsers] = useState([]);

    const per_page = useRef();
    const search = useRef();

    //perpage
    const Page = [5, 10, 20, 50];
    const [perpage] = useState(Page);
    const [isLoading, setIsLoading] = useState(true);
    const [page, setPage] = useState('');

    // pagination
    const [pageUrl, setPageUrl] = useState('http://localhost/laravel-crud-api/api/public/api/members');
    const [first, setFirst] = useState('');
    const [last, setLast] = useState('');
    const [prev, setPrev] = useState('');
    const [next, setNext] = useState('');
    const [nextPage, setNextPage] = useState('');
    const [prevPage, setPrevPage] = useState('');
    const [currentPage, setCurrentPage] = useState('');
    const [lastPage, setLastPage] = useState('');
    const [to, setTo] = useState('');
    const [from, setFrom] = useState('');
    const [total, setTotal] = useState('');


    //set projectId in state
    const [selectedUserId, setSelectedUserId] = useState(null);
    //set currentProject in state
    const [currentUser, setCurrentUser] = useState({id: null, name: '', email: ''});


    useEffect(() => {
        User();
    }, [pageUrl, first, last, prevPage, from]);


    //handlePager
    const handlePager = (event) => {
        event.preventDefault();
        let selected = event.target.value;
        if (selected !== page) {
            setPageUrl(event.target.value);
            User();
        }
    };


    //handlePage
    const handlePage = (event) => {
        event.preventDefault();
        let selected = event.target.value;
        if (selected !== page) {
            setPage(event.target.value);
            User();
        }
    };
    const User = () => {
        setIsLoading(true);
        let params = {
            keyword: search.current.value,
            per_page: per_page.current.value,
        };
        const baseUrl = 'http://localhost/laravel-crud-api/api/public/api/members';
        axios.get(baseUrl, {params: params}).then(response => {
            const user = response.data.links;
            const users = response.data.meta;
            if (response.data) {
                setIsLoading(false);
                setUsers(response.data.data || []);
                setCurrentPage(users.current_page);
                setLastPage(users.last_page);
                setFrom(users.form);
                setTo(users.to);
                setTotal(users.total);
                setFirst(user.first);
                setLast(user.last);
                setPrev(user.prev);
                setNext(user.next);
                setNextPage(users.current_page + 1);
                setPrevPage(users.current_page - 1);

            } else {
                setIsLoading(false);
            }
        }).catch(errors => {
            new CustomException('UserList', errors)
        })
    };

    //add project
    function addUser(user) {
        setIsLoading(true);
        User();
    }

    //delete users
    function deleteUser(userId) {
        setIsLoading(true);
        setUsers(users.filter(user => user.id !== userId));
        User();
    }

    function updateUser(id, updatedUser) {
        setIsLoading(true);
        setUsers(users.map(user => (user.id === id ? updatedUser : user)));
        User();
    }

    return (
        <div className="container">
            <div className="panel panel-default">
                <h1><i className="fa fa-users"/> UserLists</h1>
                <div className="panel-heading">
                </div>
                <PageLoading isLoading={isLoading}/>
                <div className="panel-body ">
                    <div className="d-flex">
                        <h4>
                            <button type="submit" className=" btn btn-primary mr-2" data-toggle="modal"
                                    data-target="#AddUserModal"><i className="fa fa-user-plus mr-1"/>Add User
                            </button>
                        </h4>
                        <h4><Link to="/" className="btn btn-primary mr-2 float-right"><i
                            className="fa fa-arrow-left"/> Back</Link></h4>
                    </div>
                    <div className="d-flex justify-content-between">
                        <div className="d-flex flex-row justify-content-center align-items-center">
                            <label className="mr-2">Perpage:</label>
                            <select ref={per_page}
                                    onChange={(event) => handlePage(event)}
                                    className="form-control">
                                {perpage.map((page, i) => {
                                    return <option key={i} defaultValue={page}>{page || 10}</option>;
                                })}
                            </select>
                        </div>
                        <div className="form-group d-flex flex-row float-right w-50">
                            <label htmlFor="search" className="mr-2 mt-2">Search:</label>
                            <input type="text" className="form-control search-input"
                                   ref={search} aria-describedby="search"
                                   onChange={User} placeholder="search"/>
                        </div>
                    </div>
                    <div>
                        {users.length > 0 ? (
                            <div>
                                <table className="table table-striped">
                                    <thead>
                                    <tr>
                                        <th scope="col"><i className="fa fa-star"/></th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Email</th>
                                        <th scope="col" className="text-center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {users.map((user, i) => {
                                        return <tr key={i}>
                                            <td>{++i}</td>
                                            <td><Link to={'users/' + user.id}>{user.name}</Link></td>
                                            <td>{user.email}</td>
                                            <td className="text-center d-flex">
                                                <Link to={'users/' + user.id} className="btn btn-primary mr-2"><i
                                                    className="fa fa-eye"/></Link>
                                                <button onClick={() => {
                                                    setCurrentUser(user);
                                                }} className="btn btn-success mr-2" data-toggle="modal"
                                                        data-target="#EditUserModal"><i className="fa fa-pen"/>
                                                </button>
                                                <button onClick={() => {
                                                    setSelectedUserId(user.id);
                                                }} className="btn btn-danger" data-toggle="modal"
                                                        data-target="#DeleteUserModal"><i className="fa fa-times"/>
                                                </button>
                                            </td>
                                        </tr>
                                    })}
                                    </tbody>
                                </table>
                                <div className=" d-flex justify-content-start"><label
                                    className="mr-2">Page {currentPage} of {lastPage}</label>
                                </div>
                                <nav>
                                    <ul className="pagination justify-content-end">
                                        <li className={prev === null ? 'page-item disabled' : 'page-item'}>
                                            <a className="page-link" href={prev} onClick={handlePager}
                                               tabIndex="-1">Previous</a>
                                        </li>
                                        <li className="page-item active">
                                            <a className="page-link" href={currentPage}
                                               onClick={handlePager}>{currentPage}<span
                                                className="sr-only">(current)</span></a>
                                        </li>
                                        <li className={to === total ? 'hide-element' : 'page-item'}><a
                                            className="page-link"
                                            onClick={handlePager}
                                            href={next}>{nextPage}</a>
                                        </li>
                                        <li className={next === null ? 'page-item disabled' : 'page-item'}>
                                            <a className="page-link" href={next} onClick={handlePager}>Next</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        ) : (
                            <tr>
                                <td colSpan={3}>No Users</td>
                            </tr>
                        )}
                    </div>
                </div>
                <AddUser addUser={addUser}/>
                <DeleteUser userId={selectedUserId} deleteUser={deleteUser}/>
                {/*<EditUser user={currentUser} updateUser={updateUser}/>*/}
            </div>
        </div>
    );
};

export default UserList;