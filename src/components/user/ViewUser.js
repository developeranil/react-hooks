import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import axios from "axios";
import PropTypes from 'prop-types';
import CustomException from "../../common/CustomException";
import PageLoading from "../loading/PageLoading";

const ViewUser = (props) => {

    const [users, setUsers] = useState([]);
    const [isLoading, setIsLoading] = useState(true);


    useEffect(() => {
        let userId = props.match.params.id;
        const baseUrl = 'http://localhost/laravel-crud-api/api/public/api/members/';
        axios.get(baseUrl + userId).then(response => {
            let users = response.data;
            if (response.data) {
                setIsLoading(false);
                setUsers(users || [])
            } else {
                setIsLoading(false);
            }

        }).catch(errors => {
            new CustomException('ViewUser', errors)
        });
    }, []);

    ViewUser.propTypes = {
        match: PropTypes.shape({
            params: PropTypes.shape({
                id: PropTypes.number
            })
        })
    };

    return (
        <div className="container">
            <div className="panel panel-default">
                <h1><i className="fa fa-user-tie"/>View User</h1>
                <div className="panel-heading">
                </div>
                <PageLoading isLoading={isLoading}/>
                <div className="panel-body ">
                    <div className="d-flex">
                        <h4><Link to="/user" className="btn btn-primary mr-2 float-right"><i
                            className="fa fa-arrow-left"/> Back</Link></h4>
                    </div>
                    <div className="table-responsive">
                        <table className="table table-hover">
                            <tbody>
                            <tr>
                                <th width="28%" scope="row" className="p-2"> Name:</th>
                                <td className="p-2">{users.name}</td>
                            </tr>
                            <tr>
                                <th scope="row" className="p-2">Email:</th>
                                <td className="p-2">{users.email}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ViewUser;