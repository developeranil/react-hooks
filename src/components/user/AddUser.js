import React, {useState, useRef} from 'react';
import PropTypes from 'prop-types';
import axios from "axios";
import ModalPageLoading from "../loading/ModalPageLoading";
import CustomException from "../../common/CustomException";

const AddUser = (props) => {
    const [isLoading, setIsLoading] = useState(false);

    const name = useRef();
    const email = useRef();


    const onSubmit = (event) => {
        setIsLoading(true);
        event.preventDefault();
        const user = {
            name: name.current.value,
            email: email.current.value
        };
        let baseUrl = 'http://localhost/laravel-crud-api/api/public/api/members';
        axios.post(baseUrl, user).then(response => {
            if (response.data) {
                setIsLoading(false);
                props.addUser();
                name.current.value = '';
                email.current.value = '';
                // close modal
                document.getElementById('btnCloseAddUserModal').click();
            }
        }).catch(errors => {
            new CustomException('AddUser', errors)
        });
    };


    AddUser.propTypes = {
        addUser: PropTypes.func,
    };
    return (
        <div className="modal fade" id="AddUserModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header bg-primary text-white">
                        <h5 className="modal-title" id="exampleModalLongTitle"><i className="fa fa-user-plus mr-2"/>Add
                            User</h5>
                        <button type="button" id="btnCloseAddUserModal" className="close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <ModalPageLoading isLoading={isLoading}/>
                        <form onSubmit={onSubmit} noValidate>
                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label htmlFor="title"> Name:</label>
                                    <input type="text" className="form-control" name="name"
                                           placeholder=" name" ref={name} required/>
                                </div>
                                <div className="form-group col-md-6">
                                    <label htmlFor="title">Email:</label>
                                    <input type="email" className="form-control" name="email"
                                           placeholder="email" ref={email} required/>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-outline-secondary" data-dismiss="modal"><i
                                    className="mr-1  fa fa-times"/>Close
                                </button>
                                <button type="submit" className="btn btn-primary"><i className="fa fa-check mr-2"/>Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default AddUser;