import React from 'react';

const Pagination = (props) => {
    return (
        <div>
            <div className=" d-flex justify-content-start"><label
                className="mr-2">Page {props.currentPage} of {props.lastPage}</label>
            </div>
            <nav aria-label="Page navigation example">
                <ul className="pagination pagination-sm justify-content-end">
                    <li className={props.prevPageUrl === null ? 'page-item disabled' : 'page-item'}>
                        <a className="page-link" onClick={props.handlePager}
                           href={props.prevPageUrl}>Previous</a>
                    </li>
                    <li className="page-item active">
                        <a className="page-link" onClick={props.handlePager}
                           href={props.currentPage}>{props.currentPage}<span
                            className="sr-only">(current)</span>
                        </a>
                    </li>
                    <li className={props.to === props.total ? 'hide-element' : 'page-item'}><a
                        className="page-link"
                        onClick={props.handlePager}
                        href={props.nextPageUrl}>{props.nextPage}</a>
                    </li>
                    <li className={props.nextPageUrl === null ? 'page-item disabled' : 'page-item'}>
                        <a className="page-link" onClick={props.handlePager}
                           href={props.nextPageUrl}>Next</a>
                    </li>
                </ul>
            </nav>
        </div>
    );
};
export default Pagination;