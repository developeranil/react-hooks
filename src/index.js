import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {BrowserRouter as Router, Route} from 'react-router-dom';

import './common/axios.config';

import Dashboard from './components/dashboard/Dashboard';
//import Member
import UserTable from './components/member/UserTable';
import ViewMember from './components/member/ViewMember';
import Login from './components/member/Login';
import MemberTable from './components/member/MemberTable';
import Profile from './components/member/Profile';
//import Project
import ProjectList from './components/project/ProjectList';
import ViewProject from './components/project/ViewProject';
// import addressBook
import AddressBookList from './components/addressBook/AddressBookList';
import ViewBook from './components/addressBook/ViewBook';
//import Contact
import ContactList from './components/contact/ContactList';
import ViewContact from './components/contact/ViewContact';
//import User
import UserList from './components/user/UserList';
import ViewUser from "./components/user/ViewUser";
// import Date
import Date from './components/DateConveter/Date';
import DateList from "./components/DateConveter/DateList";


ReactDOM.render(<Router>
        <div>
            <Route exact path='/' component={App}/>

            <Route path='/dashboard' component={Dashboard}/>

            {/*userRoutes*/}
            <Route path='/list' component={UserTable}/>
            <Route path='/user' component={UserList}/>
            <Route path='/users/:id' component={ViewUser}/>
            <Route path='/member' component={MemberTable}/>
            <Route path='/profile' component={Profile}/>
            <Route path='/member/login' component={Login}/>
            <Route path='/member/:id' component={ViewMember}/>

            {/*projectRoutes*/}
            <Route path='/project' component={ProjectList}/>
            <Route path='/view/:id' component={ViewProject}/>


            {/*addressBookRoutes*/}
            <Route path='/story' component={AddressBookList}/>
            <Route path='/stories/:id' component={ViewBook}/>


            {/*contactRoutes*/}
            <Route path='/contact' component={ContactList}/>
            <Route path='/data/:id' component={ViewContact}/>

            {/*date*/}
            <Route path='/date' component={Date}/>
            <Route path='/dates' component={DateList}/>

        </div>
    </Router>,
    document.getElementById('root')
);

