import * as firebase from 'firebase';
import 'firebase/firestore';

let config = {
  apiKey: 'AIzaSyDmklBNZAOKUoLA0Zr0zY4lj22ou10Hl6k',
  authDomain: 'react-hook-firebase.firebaseapp.com',
  databaseURL: 'https://react-hook-firebase.firebaseio.com',
  projectId: 'react-hook-firebase',
  storageBucket: 'react-hook-firebase.appspot.com',
  messagingSenderId: '833177419207'
};
firebase.initializeApp(config);
const database = firebase.firestore();
database.settings({
  timestampsInSnapshots: true
});
export default firebase ;