import React, {useState} from 'react';
import './App.css';
import Header from './components/layouts/Header';
import PageLoading from "./components/loading/PageLoading";
import Dashboard from "./components/dashboard/Dashboard";

const App = () => {
    const [isLoading, setIsLoading] = useState(false);

    return (
        <div>
            <div className="wrapper-main-content">
                <div className="panel panel-default">
                    <Header/>
                </div>
                <div className="container main-content color-gray-light pt-4">
                    <section className="com-maincontent-header d-flex w-100">
                        <Dashboard/></section>
                    <PageLoading isLoading={isLoading}/>

                </div>
            </div>
        </div>
    );
};
export default App;